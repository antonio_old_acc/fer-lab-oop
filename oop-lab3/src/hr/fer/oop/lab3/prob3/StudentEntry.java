package hr.fer.oop.lab3.prob3;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.TreeSet;

/**
 * Class {@link StudentEntry} can be used to store informations about the student.
 * @author Anthony
 * @version 1.0.0.0
 */
public class StudentEntry {
	
	private String name;
	private LinkedList<Integer> grades = new LinkedList<>();
	
	/**
	 * Creates a new {@link StudenEntry} element.
	 * @param name Student's name.
	 */
	public StudentEntry(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the student's name.
	 * @return Student's name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Gets the list of grades given to this student.
	 * @return The list of grades (unmodifiable).
	 */
	public Collection<Integer> getGrades() {
		return Collections.unmodifiableCollection(grades);
	}
	
	/**
	 * Gives a grade to the student.
	 * @param grade Grade that will be given to this student (must be an integer between 1 and 5, both inclusive).
	 */
	public void giveGrade(Integer grade) {
		if ((grade < 1) || (grade > 5)) {
			throw new RuntimeException("Grade must be an integer between 1 and 5, both inclusive.");
		}
		grades.add(grade);
	}
	
	/**
	 * Gets the number of grades of this student.
	 * @return The number of grades of this student.
	 */
	public int gradeCount() {
		return grades.size();
	}
	
	/**
	 * Gets the mean grade value of this student.
	 * @return Mean grade value of this student.
	 */
	public double getGradeMean() {
		return StatisticsUtil.getMean(grades);
	}
	
	/**
	 * Gets the median grade value of this student.
	 * @return Median grade value of this student.
	 */
	public double getGradeMedian() {
		return StatisticsUtil.getMedian(grades);
	}
	
	/**
	 * Gets the grade population standard deviation of this student.
	 * @return Grade population standard deviation of this student.
	 */
	public double getGradePopulationStdDev() {
		return StatisticsUtil.getPopulationStdDev(grades);
	}
	
	/**
	 * Gets all grades in form of a string.
	 * @return All grades.
	 */
	public String getAllGradesString() {
		
		String string = "";
		boolean firstEntry = true;
		
		for (Integer grade : grades) {
			if (firstEntry) {
				firstEntry = false;
			} else {
				string += " ";
			}
			string += grade.toString();
		}
		
		return string;
		
	}
	
	/**
	 * Gets unique grades in form of a string.
	 * @return Unique grades.
	 */
	public String getUniqueGradesString() {
		
		TreeSet<Integer> uniqueGrades = new TreeSet<>(grades);
		String string = "";
		boolean firstEntry = true;
		
		for (Integer grade : uniqueGrades) {
			if (firstEntry) {
				firstEntry = false;
			} else {
				string += " ";
			}
			string += grade.toString();
		}
		
		return string;
		
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof StudentEntry) {
			return ((StudentEntry)obj).name.equals(this.name);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
}
