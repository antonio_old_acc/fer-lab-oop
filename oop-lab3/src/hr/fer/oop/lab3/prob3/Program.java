package hr.fer.oop.lab3.prob3;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * Program for calculation the grade averages and other statistics.
 * @author Anthony
 * @version 1.0.0.0
 */
public final class Program {

	private Program() { }
	
	/**
	 * Entry method for the program class.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		String result;
		boolean running = true;
		
		HashMap<String, LinkedList<Integer>> grades = new HashMap<>(); 
		
		while (running) {
			
			result = scanner.nextLine().trim();
			
			if (result.equals("KRAJ")) {
				
				running = false;
				
			} else {
				
				while (result.contains("  ")) {
					result = result.replace("  ", " ");
				}
				
				String[] split = result.split(" ");
				
				if (split.length < 2) {
					System.err.println("Invalid input.");
					continue;
				}
				
				int grade;
				
				try {
					grade = Integer.parseInt(split[split.length - 1]);
				} catch (Exception ex) {
					System.err.println("Invalid input.");
					continue;
				}
				
				if ((grade < 1) || (grade > 5)) {
					System.err.println("Invalid grade (must be between 1 and 5, both inclusive).");
					continue;
				}
				
				String constructedName = split[0];
				
				for (int i = 1; i < split.length - 1; i++) {
					constructedName += " " + split[i];
				}
				
				LinkedList<Integer> personalGrades;
				
				if (grades.containsKey(constructedName)) {
					personalGrades = grades.get(constructedName);
				} else {
					personalGrades = new LinkedList<>();
					grades.put(constructedName, personalGrades);
				}
				
				personalGrades.add(grade);
				
			}
			
		}
		
		System.out.println();
		
		for (Map.Entry<String, LinkedList<Integer>> entry : grades.entrySet()) {
			
			System.out.println("Učenik " + entry.getKey());
			
			int gradeCount = entry.getValue().size();
			System.out.println("  Broj ocjena: " + Integer.toString(gradeCount));
			
			int gradeTotal = 0;
			System.out.print("  Ocjene:");
			for (Integer grade : entry.getValue()) {
				System.out.print(" " + Integer.toString(grade));
				gradeTotal += grade;
			}
			System.out.println();
			
			TreeSet<Integer> set = new TreeSet<>();
			set.addAll(entry.getValue());
			
			System.out.print("  Različite ocjene:");
			for (Integer grade : set) {
				System.out.print(" " + Integer.toString(grade));
			}
			System.out.println();
			
			double average = (double)gradeTotal / (double)gradeCount;
			System.out.println("  Prosječna ocjena: " + Double.toString(average));
			
			double stdDev = 0.0;
			for (Integer grade : entry.getValue()) {
				stdDev += Math.pow((double)grade - average, 2.0);
			}
			stdDev /= (double)gradeCount;
			stdDev = Math.sqrt(stdDev);
			
			System.out.println("  Standardno odstupanje: " + Double.toString(stdDev));
			
			System.out.println();
			
		}
		
		scanner.close();
		
	}
	
	/* Template testovi

Jasna 5
Ante 5
Jasna 4
Stjepan 3
Ante 5
Stjepan 4
Jasna 5
Ante 3
Jasna 4
Stjepan 4
KRAJ

	 */
	
}
