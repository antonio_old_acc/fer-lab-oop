package hr.fer.oop.lab3.prob3;

import java.util.Collection;

/**
 * This class contains static methods that can be useful when calculating statistic values for collections of numbers.
 * @author Anthony
 * @version 1.0.0.0
 */
public final class StatisticsUtil {

	private StatisticsUtil() { }
	
	/**
	 * Calculates mean value of the collection.
	 * @param collection Collection of numbers.
	 * @return Mean value of numbers.
	 */
	public static <T extends Number> double getMean(Collection<T> collection) {
		
		double sum = 0.0;
		int total = 0;
		
		for (T number : collection) {
			sum += number.doubleValue();
			total++;
		}
		
		return sum / (double)total;
		
	}
	
	/**
	 * Calculates median value of the collection.
	 * @param collection Collection of numbers.
	 * @return Median value of numbers.
	 */
	public static <T extends Number> double getMedian(Collection<T> collection) {
		
		double mean = getMean(collection);
		double offset = 0.0;
		double median = 0.0;
		boolean setFirst = true;
		
		for (T number : collection) {
			double currentOffset = Math.abs(number.doubleValue() - mean);
			if ((setFirst) || (currentOffset < offset)) {
				offset = currentOffset;
				median = number.doubleValue();
				setFirst = false;
			}
		}
		
		return median;
		
	}
	
	/**
	 * Calculates population standard deviation value of the collection.
	 * @param collection Collection of numbers.
	 * @return Population standard deviation of numbers.
	 */
	public static <T extends Number> double getPopulationStdDev(Collection<T> collection) {
		
		double mean = getMean(collection);
		double sum = 0.0;
		int total = 0;
		
		for (T number : collection) {
			sum += Math.pow(number.doubleValue() - mean, 2.0);
			total++;
		}
		
		return Math.sqrt(sum / (double)total);
		
	}
	
}
