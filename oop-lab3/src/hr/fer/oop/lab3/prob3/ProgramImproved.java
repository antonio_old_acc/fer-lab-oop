package hr.fer.oop.lab3.prob3;

import java.util.LinkedHashMap;
import java.util.Scanner;

/**
 * Program (improved, more object-oriented) for calculation the grade averages and other statistics.
 * @author Anthony
 * @version 1.0.0.0
 */
public final class ProgramImproved {

	private ProgramImproved() { }
	
	/**
	 * Entry method for the program improved class.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args) {
		
		System.out.println("PROGRAM IMPROVED!");
		
		Scanner scanner = new Scanner(System.in);
		String result;
		boolean running = true;
		
		LinkedHashMap<String, StudentEntry> map = new LinkedHashMap<>();
		
		while (running) {
			
			result = scanner.nextLine().trim();
			
			if (result.equals("KRAJ")) {
				
				running = false;
				
			} else {
				
				while (result.contains("  ")) {
					result = result.replace("  ", " ");
				}
				
				String[] split = result.split(" ");
				
				if (split.length < 2) {
					System.err.println("Invalid input.");
					continue;
				}
				
				int grade;
				
				try {
					grade = Integer.parseInt(split[split.length - 1]);
				} catch (Exception ex) {
					System.err.println("Invalid input.");
					continue;
				}
				
				if ((grade < 1) || (grade > 5)) {
					System.err.println("Invalid grade (must be between 1 and 5, both inclusive).");
					continue;
				}
				
				String constructedName = split[0];
				
				for (int i = 1; i < split.length - 1; i++) {
					constructedName += " " + split[i];
				}
				
				StudentEntry student;
				
				if (map.containsKey(constructedName)) {
					student = map.get(constructedName);
				} else {
					student = new StudentEntry(constructedName);
					map.put(constructedName, student);
				}
				
				student.giveGrade(grade);
				
			}
			
		}
		
		System.out.println();
		
		scanner.close();
		
		for (StudentEntry student : map.values()) {
			
			System.out.println("Učenik " + student.getName());
			System.out.println("  Broj ocjena: " + student.gradeCount());
			System.out.println("  Ocjene: " + student.getAllGradesString());
			System.out.println("  Različite ocjene: " + student.getUniqueGradesString());
			System.out.println("  Prosječna ocjena: " + student.getGradeMean());
			System.out.println("  Standardno odstupanje: " + student.getGradePopulationStdDev());
			System.out.println();
			
		}
		
	}
	
}
