package hr.fer.oop.lab3.prob1;

import java.awt.Point;
import java.util.LinkedList;

import hr.fer.oop.lab3.pic.Picture;

public class PictureElements implements Drawable {

	private LinkedList<Drawable> elements = new LinkedList<>();
	
	public void addElements(Drawable e) {
		elements.add(e);
	}
	
	@Override
	public void drawOnPicture(Picture picture) {
		for (Drawable d : elements) {
			d.drawOnPicture(picture);
		}
	}

	public static void main(String[] args) {
		
		Picture p = new Picture(100, 100);
		Triangle t = new Triangle(new Point(10, 10), new Point(40, 10), new Point(10, 40));
		Triangle t2 = new Triangle(new Point(40, 40), new Point(80, 40), new Point(30, 80));
		
		PictureElements pe = new PictureElements();
		pe.addElements(t);
		pe.addElements(t2);
		
		pe.drawOnPicture(p);
		
		
		p.renderImageToStream(System.out);
	}
	
}
