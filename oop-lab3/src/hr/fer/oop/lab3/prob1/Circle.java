package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.Picture;

/**
 * Class {@link Circle} is a visual representation of a circle that can be drawn on {@link Picture}.
 * @author Anthony
 * @version 1.0.0.0
 * @see CircleFast
 */
public class Circle extends Ellipse {

	/**
	 * Creates a {@link Circle} from a point and a radius.
	 * @param x Coordinate X of the center.
	 * @param y Coordinate Y of the center.
	 * @param radius Circle radius.
	 */
	public Circle(int x, int y, int radius) {
		super(x, y, radius, radius);
	}

	/**
	 * Creates a duplicate of a {@link Circle}.
	 * @param original An original {@link Circle} whose duplicate will be made.
	 */
	public Circle(Circle original) {
		super(original);
	}
	
	/**
	 * Gets the circle radius.
	 * @return Circle radius.
	 */
	public int getRadius() {
		return a;
	}
	
	/**
	 * Sets the circle radius.
	 * @param radius New circle radius.
	 */
	public void setRadius(int radius) {
		if (radius <= 0) {
			throw new RuntimeException("Radius must be a positive integer.");
		}
		a = radius;
		b = radius;
	}
	
	@Override
	public void setA(int a) {
		setRadius(a);
	}
	
	@Override
	public void setB(int b) {
		setRadius(b);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Circle) {
			return super.equals(obj);
		}
		return false;
	}
	
}
