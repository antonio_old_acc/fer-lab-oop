package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.Picture;

public class BorderedPicture extends Picture {

	private int localWidth, localHeight;
	
	public BorderedPicture(int width, int height) {
		super(width + 2, height + 2);
		this.localWidth = width;
		this.localHeight = height;
		super.drawLine(0, width + 1, 0);
		super.drawLine(0, width + 1, height + 1);
		for (int i = 1; i < height + 1; i++) {
			super.turnPixelOn(0, i);
			super.turnPixelOn(width + 1, i);
		}
	}

	@Override
	public void drawLine(int x0, int x1, int y) {
		if ((x0 < 0) || (x0 >= localWidth) || (x1 < 0) || (x1 >= localWidth) || (y < 0) || (y >= localHeight)) {
			throw new RuntimeException("Coordinate is out of bounds.");
		}
		super.drawLine(x0 + 1, x1 + 1, y + 1);
	}
	
	@Override
	public void turnPixelOn(int x, int y) {
		if ((x < 0) || (x >= localWidth) || (y < 0) || (y >= localHeight)) {
			throw new RuntimeException("Coordinate is out of bounds.");
		}
		super.turnPixelOn(x + 1, y + 1);
	}
	
	@Override
	public void turnPixelOff(int x, int y) {
		if ((x < 0) || (x >= localWidth) || (y < 0) || (y >= localHeight)) {
			throw new RuntimeException("Coordinate is out of bounds.");
		}
		super.turnPixelOff(x + 1, y + 1);
	}
	
}
