package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.Picture;

/**
 * Class {@link Ellipse} is a visual representation of a ellipse that can be drawn on {@link Picture}.
 * @author Anthony
 * @version 1.0.0.0
 * @see EllipseFast
 */
public class Ellipse implements Drawable {

	protected int x, y;
	protected int a, b;
	
	/**
	 * Creates a {@link Ellipse} from a point and two radiuses.
	 * @param x Coordinate X of the center.
	 * @param y Coordinate Y of the center.
	 * @param a Horizontal radius.
	 * @param b Vertical radius.
	 */
	public Ellipse(int x, int y, int a, int b) {
		setCenter(x, y);
		setA(a);
		setB(b);
	}
	
	/**
	 * Creates a duplicate of a {@link Ellipse}.
	 * @param original An original {@link Ellipse} whose duplicate will be made.
	 */
	public Ellipse(Ellipse original) {
		this(original.x, original.y, original.a, original.b);
	}
	
	/**
	 * Gets the coordinate X of the center.
	 * @return Coordinate X of the center.
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Sets the coordinate X for the center.
	 * @param x Coordinate X for the center.
	 */
	public void setX(int x) {
		this.x = x;
	}
	
	/**
	 * Gets the coordinate Y of the center.
	 * @return Coordinate Y of the center.
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Sets the coordinate Y for the center.
	 * @param y Coordinate Y for the center.
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	/**
	 * Gets the horizontal radius.
	 * @return Horizontal radius.
	 */
	public int getA() {
		return a;
	}
	
	/**
	 * Sets the horizontal radius.
	 * @param a Horizontal radius (value must be a positive integer).
	 */
	public void setA(int a) {
		if (a <= 0) {
			throw new RuntimeException("Horizontal radius must be a positive integer.");
		}
		this.a = a;
	}
	
	/**
	 * Gets the vertical radius.
	 * @return Vertical radius.
	 */
	public int getB() {
		return b;
	}
	
	/**
	 * Sets the vertical radius.
	 * @param b Vertical radius (value must be a positive integer).
	 */
	public void setB(int b) {
		if (b <= 0) {
			throw new RuntimeException("Vertical radius must be a positive integer.");
		}
		this.b = b;
	}
	
	/**
	 * Sets the coordinates for the center.
	 * @param x Coordinate X for the center.
	 * @param y Coordinate Y for the center.
	 */
	public void setCenter(int x, int y) {
		setX(x);
		setY(y);
	}
	
	@Override
	public void drawOnPicture(Picture picture) {
		
		int width = picture.getWidth();
		int height = picture.getHeight();
		
		for (int x = 0; x < width; x++) {
			
			for (int y = 0; y < height; y++) {
				
				// Checks whether the point is inside the ellipse
				if (Math.pow(((double)x - (double)this.x) / (double)a, 2.0) + Math.pow(((double)y - (double)this.y) / (double)b, 2.0) <= 1) {
					picture.turnPixelOn(x, y);
				}
				
			}
			
		}
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Ellipse) {
			Ellipse other = (Ellipse)obj;
			return ((x == other.x) && (y == other.y) && (a == other.a) && (b == other.b));
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return 
				(Integer.valueOf(x).hashCode() << 24) ^
				(Integer.valueOf(y).hashCode() << 16) ^
				(Integer.valueOf(a).hashCode() << 8) ^
				Integer.valueOf(b).hashCode();
	}
	
}
