package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.Picture;

/**
 * Class {@link Rectangle} is a visual representation of a rectangle that can be drawn on {@link Picture}.
 * @author Anthony
 * @version 1.0.0.0
 * @see RectangleFast
 */
public class Rectangle implements Drawable {

	protected int x1, y1;
	protected int x2, y2;
	
	/**
	 * Creates a {@link Rectangle} from two points.
	 * @param x1 Coordinate X of the top-left point.
	 * @param y1 Coordinate Y of the top-left point.
	 * @param x2 Coordinate X of the bottom-right point.
	 * @param y2 Coordinate Y of the bottom-right point.
	 */
	public Rectangle(int x1, int y1, int x2, int y2) {
		setTopLeft(x1, y1);
		setBottomRight(x2, y2);
	}
	
	/**
	 * Creates a duplicate of a {@link Rectangle}.
	 * @param original An original {@link Rectangle} whose duplicate will be made.
	 */
	public Rectangle(Rectangle original) {
		this(original.x1, original.y1, original.x2, original.y2);
	}
	
	/**
	 * Gets the coordinate X of the top-left point.
	 * @return Coordinate X of the top-left point.
	 */
	public int getX1() {
		return x1;
	}
	
	/**
	 * Sets the coordinate X for the top-left point.
	 * @param x1 Coordinate X for the top-left point.
	 */
	public void setX1(int x1) {
		this.x1 = x1;
	}
	
	/**
	 * Gets the coordinate Y of the top-left point.
	 * @return Coordinate Y of the top-left point.
	 */
	public int getY1() {
		return y1;
	}
	
	/**
	 * Sets the coordinate Y for the top-left point.
	 * @param y1 Coordinate Y for the top-left point.
	 */
	public void setY1(int y1) {
		this.y1 = y1;
	}
	
	/**
	 * Gets the coordinate X of the bottom-right point.
	 * @return Coordinate X of the bottom-right point.
	 */
	public int getX2() {
		return x2;
	}
	
	/**
	 * Sets the coordinate X for the bottom-right point.
	 * @param x2 Coordinate X for the bottom-right point.
	 */
	public void setX2(int x2) {
		this.x2 = x2;
	}
	
	/**
	 * Gets the coordinate Y of the bottom-right point.
	 * @return Coordinate Y of the bottom-right point.
	 */
	public int getY2() {
		return y2;
	}
	
	/**
	 * Sets the coordinate Y for the bottom-right point.
	 * @param y2 Coordinate Y for the bottom right point.
	 */
	public void setY2(int y2) {
		this.y2 = y2;
	}
	
	/**
	 * Sets the coordinates for the top-left point.
	 * @param x Coordinate X for the top-left point.
	 * @param y Coordinate Y for the top-left point.
	 */
	public void setTopLeft(int x, int y) {
		setX1(x);
		setY1(y);
	}
	
	/**
	 * Sets the coordinates for the bottom-right point.
	 * @param x Coordinate X for the bottom-right point.
	 * @param y Coordinate Y for the bottom-right point.
	 */
	public void setBottomRight(int x, int y) {
		setX2(x);
		setY2(y);
	}
	
	@Override
	public void drawOnPicture(Picture picture) {
		
		int width = picture.getWidth();
		int height = picture.getHeight();
		
		for (int x = 0; x < width; x++) {
			
			for (int y = 0; y < height; y++) {
				
				// Checks whether the point is inside the rectangle
				if ((x >= x1) && (x <= x2) && (y >= y1) && (y <= y2)) {
					picture.turnPixelOn(x, y);
				}
				
			}
			
		}
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Rectangle) {
			Rectangle other = (Rectangle)obj;
			return ((x1 == other.x1) && (y1 == other.y1) && (x2 == other.x2) && (y2 == other.y2));
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return 
				(Integer.valueOf(x1).hashCode() << 24) ^
				(Integer.valueOf(y1).hashCode() << 16) ^
				(Integer.valueOf(x2).hashCode() << 8) ^
				Integer.valueOf(y2).hashCode();
	}

}
