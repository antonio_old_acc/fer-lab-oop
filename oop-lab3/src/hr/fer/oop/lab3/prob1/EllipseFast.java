package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.Picture;

/**
 * A variant of a {@link Ellipse} with faster drawing implementation. Instead of checking each pixel of {@link Picture}, only the ones filled will be drawn as lines.
 * @author Anthony
 * @version 1.0.0.0
 * @see Ellipse
 */
public class EllipseFast extends Ellipse {

	/**
	 * Creates a {@link EllipseFast} from a point and two radiuses.
	 * @param x Coordinate X of the center.
	 * @param y Coordinate Y of the center.
	 * @param a Horizontal radius.
	 * @param b Vertical radius.
	 */
	public EllipseFast(int x, int y, int a, int b) {
		super(x, y, a, b);
	}
	
	/**
	 * Creates a duplicate of a {@link Ellipse} as a {@link EllipseFast} object.
	 * @param original An original {@link Ellipse} whose duplicate will be made.
	 */
	public EllipseFast(Ellipse original) {
		super(original);
	}
	
	@Override
	public void drawOnPicture(Picture picture) {
		
		int width = picture.getWidth();
		int height = picture.getHeight();
		
		// Iterate vertically from -b to b
		for (int y = -b; y <= b; y++) {
			
			// Coordinate Y of the ellipse translated into the picture's local coordinate Y
			int localY = y + this.y;
			
			// Only draw the local coordinate Y if it will be visible on the picture
			if ((localY >= 0) && (localY < height)) {
				
				// Calculate the half of the ellipse secant that is horizontal and passes trough the coordinate Y
				int limitX = (int)((double)a * Math.sqrt(1.0 - Math.pow((double)y / (double)b, 2.0)));
				
				// The actual points of the line that may exceed the visible scope
				int startX = this.x - limitX;
				int endX = this.x + limitX;
				
				// If both the points of the ellipse line exceed the visible scope (on the same side), the line won't be visible
				if (((startX >= width) && (endX >= width)) || ((startX < 0) && (endX < 0))) {
					continue;
				}
				
				// Calculate the farthest visible X coordinates of the line
				startX = Math.min(width - 1, Math.max(0, startX));
				endX = Math.min(width - 1, Math.max(0, endX));
				
				picture.drawLine(startX, endX, localY);
				
			}
			
		}
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EllipseFast) {
			return super.equals(obj);
		}
		return false;
	}
	
}
