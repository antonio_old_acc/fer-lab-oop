package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.Picture;

/**
 * A variant of a {@link Rectangle} with faster drawing implementation. Instead of checking each pixel of {@link Picture}, only the ones filled will be drawn as lines.
 * @author Anthony
 * @version 1.0.0.0
 * @see Rectangle
 */
public class RectangleFast extends Rectangle {

	/**
	 * Creates a {@link RectangleFast} from two points.
	 * @param x1 Coordinate X of the top-left point.
	 * @param y1 Coordinate Y of the top-left point.
	 * @param x2 Coordinate X of the bottom-right point.
	 * @param y2 Coordinate Y of the bottom-right point.
	 */
	public RectangleFast(int x1, int y1, int x2, int y2) {
		super(x1, y1, x2, y2);
	}
	
	/**
	 * Creates a duplicate of a {@link Rectangle} as a {@link RectangleFast} object.
	 * @param original An original {@link Rectangle} whose duplicate will be made.
	 */
	public RectangleFast(Rectangle original) {
		super(original);
	}

	@Override
	public void drawOnPicture(Picture picture) {
		
		int width = picture.getWidth();
		int height = picture.getHeight();
		
		// If both the points of the rectangle exceed the visible scope (on the same side), the rectangle won't be visible
		if (((x1 >= width) && (x2 >= width)) || ((x1 < 0) && (x2 < 0))) {
			return;
		}
		
		// Calculate the farthest visible X coordinates
		int visibleX1 = Math.min(width - 1, Math.max(0, x1));
		int visibleX2 = Math.min(width - 1, Math.max(0, x2));
		
		// Calculate the farthest upper and farthest lower Y coordinates of the rectangle
		int startY = Math.min(y1, y2);
		int endY = Math.max(y1, y2);
		
		for (int y = startY; y <= endY; y++) {
			
			if ((y >= 0) && (y < height)) {
			
				picture.drawLine(visibleX1, visibleX2, y);
			
			}
			
		}
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof RectangleFast) {
			return super.equals(obj);
		}
		return false;
	}
	
}
