package hr.fer.oop.lab3.prob1;

import java.awt.Point;

import hr.fer.oop.lab3.pic.Picture;

public class Triangle implements Drawable {

	Point a, b, c;
	
	public Triangle(Point a, Point b, Point c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	@Override
	public void drawOnPicture(Picture picture) {
		for (int x = 0; x < picture.getWidth(); x++) {
			for (int y = 0; y < picture.getHeight(); y++) {
				if (contains(x, y)) {
					picture.turnPixelOn(x, y);
				}
			}
		}
	}
	
	public boolean contains(Point p) {
		return contains(p.x, p.y);
	}
	
	public boolean contains(int x, int y) {

		double ABC = Math.abs (a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y));
		double ABP = Math.abs (a.x * (b.y - y) + b.x * (y - a.y) + x * (a.y - b.y));
		double APC = Math.abs (a.x * (y - c.y) + x * (c.y - a.y) + c.x * (a.y - y));
		double PBC = Math.abs (x * (b.y - c.y) + b.x * (c.y - y) + c.x * (y - b.y));

		return ABP + APC + PBC == ABC;
	}
	
	public static void main(String[] args) {
		Picture p = new Picture(100, 100);
		Triangle t = new Triangle(new Point(10, 10), new Point(80, 10), new Point(10, 80));
		t.drawOnPicture(p);
		p.renderImageToStream(System.out);
	}

}
