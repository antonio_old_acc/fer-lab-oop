package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.Picture;

/**
 * A variant of a {@link Circle} with faster drawing implementation. Instead of checking each pixel of {@link Picture}, only the ones filled will be drawn as lines.
 * @author Anthony
 * @version 1.0.0.0
 * @see Circle
 */
public class CircleFast extends EllipseFast {

	/**
	 * Creates a {@link CircleFast} from a point and a radius.
	 * @param x Coordinate X of the center.
	 * @param y Coordinate Y of the center.
	 * @param radius Circle radius.
	 */
	public CircleFast(int x, int y, int radius) {
		super(x, y, radius, radius);
	}
	
	/**
	 * Creates a duplicate of a {@link Circle} as a {@link CircleFast} object.
	 * @param original An original {@link Circle} whose duplicate will be made.
	 */
	public CircleFast(Circle original) {
		super(original);
	}
	
	/**
	 * Gets the circle radius.
	 * @return Circle radius.
	 */
	public int getRadius() {
		return a;
	}
	
	/**
	 * Sets the circle radius.
	 * @param radius New circle radius.
	 */
	public void setRadius(int radius) {
		if (radius <= 0) {
			throw new RuntimeException("Radius must be a positive integer.");
		}
		a = radius;
		b = radius;
	}
	
	@Override
	public void setA(int a) {
		setRadius(a);
	}
	
	@Override
	public void setB(int b) {
		setRadius(b);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CircleFast) {
			return super.equals(obj);
		}
		return false;
	}
	
}
