package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.Picture;
import hr.fer.oop.lab3.pic.PictureDisplay;

/**
 * Demonstration class for the first problem of the third OOP lab.
 * @author Anthony
 * @version 1.0.0.0
 */
public final class Demonstration {
	
	private static boolean SHOW_DISPLAY = false;
	
	private Demonstration() { }
	
	/**
	 * Entry method for the demonstration class.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args) {
		
		Picture rectPicture = new Picture(100, 50);
		
		Rectangle rect = new Rectangle(10, 10, 45, 40);
		RectangleFast rectFast = new RectangleFast(55, 10, 90, 40);
		
		rect.drawOnPicture(rectPicture);
		rectFast.drawOnPicture(rectPicture);
		
		System.out.println("Rendering rectangles:");
		rectPicture.renderImageToStream(System.out);
		System.out.println();
		
		Picture circPicture = new Picture(100, 50);
		
		Circle circ = new Circle(25, 25, 15);
		CircleFast circFast = new CircleFast(75, 25, 15);
		
		circ.drawOnPicture(circPicture);
		circFast.drawOnPicture(circPicture);
		
		System.out.println("Rendering circles:");
		circPicture.renderImageToStream(System.out);
		System.out.println();
		
		Picture triPicture = new Picture(100, 50);
		
		EquilateralTriangle tri = new EquilateralTriangle(10, 45, 40);
		EquilateralTriangleFast triFast = new EquilateralTriangleFast(55, 90, 40);
		
		tri.drawOnPicture(triPicture);
		triFast.drawOnPicture(triPicture);
		
		System.out.println("Rendering equilateral triangles:");
		triPicture.renderImageToStream(System.out);
		System.out.println();
		
		System.out.println("All done.");
		
		if (SHOW_DISPLAY) {
			PictureDisplay.showPicture(rectPicture, 5);
			PictureDisplay.showPicture(circPicture, 5);
			PictureDisplay.showPicture(triPicture, 5);
		}
		
	}

}

