package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.Picture;

/**
 * Interface {@link Drawable} can be implemented in classes that can be visually represented, allowing them to be drawn on a {@link Picture}.
 * @author Anthony
 * @version 1.0.0.0
 */
public interface Drawable {

	/**
	 * Draws this object on a {@link Picture}.
	 * @param picture A {@link Picture} on which the drawing will be done.
	 */
	public void drawOnPicture(Picture picture);
	
}
