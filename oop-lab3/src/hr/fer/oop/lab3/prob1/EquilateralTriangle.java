package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.Picture;

/**
 * Class {@link EquilateralTriangle} is a visual representation of a equilateral triangle that can be drawn on {@link Picture}.
 * @author Anthony
 * @version 1.0.0.0
 * @see EquilateralTriangleFast
 */
public class EquilateralTriangle implements Drawable {

	protected int x1, x2, y;
	
	/**
	 * Creates a {@link EquilateralTriangle} from two points that lay on the same horizontal line. The line trough those two points will be used as a base and the traingle will extend upwards.
	 * @param x1 Coordinate X for the first point.
	 * @param x2 Coordinate X for the second point.
	 * @param y Coordinate Y for both of the points.
	 */
	public EquilateralTriangle(int x1, int x2, int y) {
		setX1(x1);
		setX2(x2);
		setY(y);
	}
	
	/**
	 * Creates a duplicate of a {@link EquilateralTriangle}.
	 * @param original An original {@link EquilateralTriangle} whose duplicate will be made.
	 */
	public EquilateralTriangle(EquilateralTriangle original) {
		this(original.x1, original.x2, original.y);
	}
	
	/**
	 * Gets the coordinate X of the first point.
	 * @return Coordinate X of the first point.
	 */
	public int getX1() {
		return x1;
	}
	
	/**
	 * Sets the coordinate X for the first point.
	 * @param x1 Coordinate X for the first point.
	 */
	public void setX1(int x1) {
		this.x1 = x1;
	}
	
	/**
	 * Gets the coordinate X of the second point.
	 * @return Coordinate X of the second point.
	 */
	public int getX2() {
		return x2;
	}
	
	/**
	 * Sets the coordinate X for the second point.
	 * @param x2 Coordinate X for the second point.
	 */
	public void setX2(int x2) {
		this.x2 = x2;
	}
	
	/**
	 * Gets the coordinate Y of both points.
	 * @return Coordinate Y of both points.
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Sets the coordinate Y for both of the points.
	 * @param y Coordinate Y for both of the points.
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	@Override
	public void drawOnPicture(Picture picture) {
		
		int width = picture.getWidth();
		int height = picture.getHeight();
		
		// Calculates which of the both X coordinates of the base is lesser and which is greater
		int lesserX = Math.min(x1, x2);
		int greaterX = Math.max(x1, x2);
		
		// Calculates base length and height
		int baseLength = greaterX - lesserX;
		double baseHeight = (double)baseLength * Math.sqrt(3.0) / 2.0;
		
		// Calculates the coordinates of the top point
		double topX = (double)x1 + (double)baseLength / 2.0;
		double topY = (double)this.y - baseHeight;
		
		for (int x = 0; x < width; x++) {
			
			for (int y = 0; y < height; y++) {
				
				// Checks whether the point is in-between the top-most and the bottom-most Y coordinate
				if ((y <= this.y) && (y >= topY)) {
					
					// Calculates the half of the length between the two points on the sides of the triangle at that height
					double currentLengthHalf = ((double)y - topY) / Math.sqrt(3.0);
					
					// Check if the point lays on that line
					if (Math.abs(x - topX) <= currentLengthHalf) {
						picture.turnPixelOn(x, y);
					}
					
				}
				
			}
			
		}
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EquilateralTriangle) {
			EquilateralTriangle other = (EquilateralTriangle)obj;
			return ((x1 == other.x1) && (x2 == other.x2) && (y == other.y));
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return 
				(Integer.valueOf(x1).hashCode() << 20) ^
				(Integer.valueOf(x2).hashCode() << 10) ^
				Integer.valueOf(y).hashCode();
	}
	
}
