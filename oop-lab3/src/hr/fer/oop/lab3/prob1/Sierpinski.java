package hr.fer.oop.lab3.prob1;

import java.awt.Point;

import hr.fer.oop.lab3.pic.Picture;

public class Sierpinski extends PictureElements {

	public Triangle t;
	
	public Sierpinski(Point a, Point b, Point c, int level) {
		t = new Triangle(a, b, c);
		calculateTriangles(level, a, b, c);
	}
	
	private void calculateTriangles(int level, Point a, Point b, Point c) {
		if (level <= 1) {
			addElements(new Triangle(a, b, c));
		} else {
			Point ab = new Point((a.x + b.x) / 2, (a.y + b.y) / 2);
			Point bc = new Point((b.x + c.x) / 2, (b.y + c.y) / 2);
			Point ac = new Point((a.x + c.x) / 2, (a.y + c.y) / 2);
			calculateTriangles(level - 1, a, ab, ac);
			calculateTriangles(level - 1, b, ab, bc);
			calculateTriangles(level - 1, c, bc, ac);
		}
	}
	
	public static void main(String[] args) {
		
		Picture p = new Picture(100, 100);
		Triangle t = new Triangle(new Point(10, 80), new Point(80, 80), new Point(40, 10));
		Sierpinski s = new Sierpinski(t.a, t.b, t.c, 2);
		s.drawOnPicture(p);
		p.renderImageToStream(System.out);
		
	}
	
}
