package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.PictureDisplay;

public class MainLab {

	public static void main(String[] args) {
		
		BorderedPicture pic = new BorderedPicture(10, 10);
		
		Circle c = new Circle(4, 4, 3);
		c.drawOnPicture(pic);
		
		pic.renderImageToStream(System.out);
		
		System.out.println();
		
		BorderedPicture pic2 = new BorderedPicture(10, 10);
		
		EquilateralTriangle t = new EquilateralTriangle(2, 6, 6);
		t.drawOnPicture(pic2);
		
		pic2.renderImageToStream(System.out);
		
		System.out.println();
		
		BorderedPicture pic3 = new BorderedPicture(50, 50);
		
		Circle c2 = new Circle(24, 24, 15);
		c2.drawOnPicture(pic3);
		
		pic3.renderImageToStream(System.out);
		
		System.out.println();
		
		BorderedPicture pic4 = new BorderedPicture(50, 50);
		
		EquilateralTriangle t2 = new EquilateralTriangle(10, 35, 40);
		t2.drawOnPicture(pic4);
		
		pic4.renderImageToStream(System.out);
		
		System.out.println();
		
		BorderedPicture pic5 = new BorderedPicture(100, 100);
		
		Circle c3 = new Circle(49, 49, 30);
		c3.drawOnPicture(pic5);
		
		pic5.renderImageToStream(System.out);
		
		System.out.println();
		
		BorderedPicture pic6 = new BorderedPicture(100, 100);
		
		EquilateralTriangle t3 = new EquilateralTriangle(20, 70, 70);
		t3.drawOnPicture(pic6);
		
		pic6.renderImageToStream(System.out);
		
		// PictureDisplay.showPicture(pic, 5);
		
	}
	
}
