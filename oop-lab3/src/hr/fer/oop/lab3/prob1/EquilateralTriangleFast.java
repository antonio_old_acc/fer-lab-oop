package hr.fer.oop.lab3.prob1;

import hr.fer.oop.lab3.pic.Picture;

/**
 * A variant of a {@link EquilateralTriangle} with faster drawing implementation. Instead of checking each pixel of {@link Picture}, only the ones filled will be drawn as lines.
 * @author Anthony
 * @version 1.0.0.0
 * @see EquilateralTriangle
 */
public class EquilateralTriangleFast extends EquilateralTriangle {

	/**
	 * Creates a {@link EquilateralTriangleFast} from two points that lay on the same horizontal line. The line trough those two points will be used as a base and the traingle will extend upwards.
	 * @param x1 Coordinate X for the first point.
	 * @param x2 Coordinate X for the second point.
	 * @param y Coordinate Y for both of the points.
	 */
	public EquilateralTriangleFast(int x1, int x2, int y) {
		super(x1, x2, y);
	}

	/**
	 * Creates a duplicate of a {@link EquilateralTriangle} as a {@link EquilateralTriangleFast} object.
	 * @param original An original {@link EquilateralTriangle} whose duplicate will be made.
	 */
	public EquilateralTriangleFast(EquilateralTriangle original) {
		super(original);
	}
	
	@Override
	public void drawOnPicture(Picture picture) {
		
		int width = picture.getWidth();
		int height = picture.getHeight();
		
		// Calculates which of the both X coordinates of the base is lesser and which is greater
		int lesserX = Math.min(x1, x2);
		int greaterX = Math.max(x1, x2);
		
		// Calculates base length and height
		int baseLength = greaterX - lesserX;
		double baseHeight = (double)baseLength * Math.sqrt(3.0) / 2.0;
		
		// Calculates the coordinates of the top point
		double topX = (double)x1 + (double)baseLength / 2.0;
		double topY = (double)this.y - baseHeight;
		
		// Iterate from top point to base
		for (int y = (int)topY; y <= this.y; y++) {
			
			if ((y >= 0) && (y < height)) {
				
				// Calculates the half of the length between the two points on the sides of the triangle at that height
				double currentLengthHalf = ((double)y - topY) / Math.sqrt(3.0);
				
				// The actual points of the line that may exceed the visible scope
				int startX = (int)(topX - currentLengthHalf);
				int endX = (int)(topX + currentLengthHalf);
				
				// If both the points of the triangle line exceed the visible scope (on the same side), the line won't be visible
				if (((startX >= width) && (endX >= width)) || ((startX < 0) && (endX < 0))) {
					continue;
				}
				
				// Calculate the farthest visible X coordinates of the line
				startX = Math.min(width - 1, Math.max(0, startX));
				endX = Math.min(width - 1, Math.max(0, endX));
				
				// If the X coordinates are equal, don't draw it (for making the fast draw look more like the regular draw)
				if (startX == endX) {
					continue;
				}
				
				endX--; // Reduce the X coordinate of the point on the right by 1 (for making the fast draw look more like the regular draw)
				
				picture.drawLine(startX, endX, y);
				
			}
			
		}
		
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EquilateralTriangleFast) {
			return super.equals(obj);
		}
		return false;
	}
	
}
