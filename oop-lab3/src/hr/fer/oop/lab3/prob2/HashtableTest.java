package hr.fer.oop.lab3.prob2;

/**
 * Class that is used to test {@link SimpleHashtable}.
 * @author Anthony
 * @version 1.0.0.0
 */
public final class HashtableTest {

	private HashtableTest() { }
	
	/**
	 * Entry method for the test class.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args) {
		
		// Create the grade hash-table
		SimpleHashtable<String, Integer> gradeList = new SimpleHashtable<>();
		
		// Put together a simple list of 4 items
		gradeList.put("Ivan", 5);
		gradeList.put("Jure", 3);
		gradeList.put("Mate", 1);
		gradeList.put("Chobbs", 2);
		
		// Check if there are 4 items
		if (gradeList.size() != 4) {
			System.err.println("ERROR: Size must be 4!");
			System.exit(-1);
		}
		
		// Validate Ivan
		if (!gradeList.containsKey("Ivan")) {
			System.err.println("ERROR: Ivan must be in a list!");
			System.exit(-1);
		} else if (gradeList.get("Ivan") != 5) {
			System.err.println("ERROR: Ivan must have grade 5!");
			System.exit(-1);
		}
		
		// Validate Jure
		if (!gradeList.containsKey("Jure")) {
			System.err.println("ERROR: Jure must be in a list!");
			System.exit(-1);
		} else if (gradeList.get("Jure") != 3) {
			System.err.println("ERROR: Jure must have grade 3!");
			System.exit(-1);
		}
		
		// Validate Mate
		if (!gradeList.containsKey("Mate")) {
			System.err.println("ERROR: Mate must be in a list!");
			System.exit(-1);
		} else if (gradeList.get("Mate") != 1) {
			System.err.println("ERROR: Mate must have grade 1!");
			System.exit(-1);
		}
		
		// Validate Mate
		if (!gradeList.containsKey("Chobbs")) {
			System.err.println("ERROR: Chobbs must be in a list!");
			System.exit(-1);
		} else if (gradeList.get("Chobbs") != 2) {
			System.err.println("ERROR: Chobbs must have grade 2!");
			System.exit(-1);
		}
		
		// Remove Mate from the list
		gradeList.remove("Mate");
		
		// Check if there are 3 items
		if (gradeList.size() != 3) {
			System.err.println("ERROR: Size must be 3!");
			System.exit(-1);
		}
		
		// Validate Mate's removal
		if (gradeList.containsKey("Mate")) {
			System.err.println("ERROR: Mate shouldn't be in the list!");
			System.exit(-1);
		}
		
		// Overwrite Jure's grade with 4
		gradeList.put("Jure", 4);
		
		// Check if there are 3 items
		if (gradeList.size() != 3) {
			System.err.println("ERROR: Size must be 3!");
			System.exit(-1);
		}
		
		// Validate Jure
		if (!gradeList.containsKey("Jure")) {
			System.err.println("ERROR: Jure must be in a list!");
			System.exit(-1);
		} else if (gradeList.get("Jure") != 4) {
			System.err.println("ERROR: Jure must have grade 4!");
			System.exit(-1);
		}
		
		// Check for grade 3 as value
		if (gradeList.containsValue(3)) {
			System.err.println("ERROR: Grade 3 must be in the table!");
			System.exit(-1);
		}
		
		// Check for grade 5 as value
		if (!gradeList.containsValue(5)) {
			System.err.println("ERROR: Grade 5 must be in the table!");
			System.exit(-1);
		}
		
		// Check for grade 4 as value
		if (!gradeList.containsValue(4)) {
			System.err.println("ERROR: Grade 4 must be in the table!");
			System.exit(-1);
		}
		
		// Check for grade 5 as value
		if (!gradeList.containsValue(2)) {
			System.err.println("ERROR: Grade 2 must be in the table!");
			System.exit(-1);
		}
		
		// Get random value
		if (gradeList.get("RandomKey") != null) {
			System.err.println("ERROR: RandomKey entry doesn't exist!");
			System.exit(-1);
		}
		
		System.out.println("Test OK.");
		
	}
	
}
