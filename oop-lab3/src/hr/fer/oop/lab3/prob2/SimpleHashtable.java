package hr.fer.oop.lab3.prob2;

/**
 * Generic class {@link SimpleHashtable} is used to store objects linked to their unique key.
 * @author Anthony
 * @version 1.0.0.0
 * @param <K> Key object type.
 * @param <V> Value object type.
 */
public class SimpleHashtable<K, V> {
	
	private static class TableEntry<K, V> {
		
		private K key;
		private V value;
		private TableEntry<K, V> next;
		
		public TableEntry(K key, V value) {
			this(key, value, null);
		}
		
		public TableEntry(K key, V value, TableEntry<K, V> next) {
			if (key == null) {
				throw new RuntimeException("Key cannot be null.");
			}
			this.key = key;
			this.value = value;
			this.next = next;
		}
		
		public K getKey() {
			return key;
		}
		
		public V getValue() {
			return value;
		}
		
		public void setValue(V value) {
			this.value = value;
		}
		
		private TableEntry<K, V> getNext() {
			return next;
		}
		
		private void setNext(TableEntry<K, V> next) {
			this.next = next;
		}
		
		@Override
		public String toString() {
			return key.toString() + ": " + value.toString();
		}
		
	}
	
	private static final int DEFAULT_HASH_SLOTS = 16;
	
	private TableEntry<K, V>[] table;
	private int size;
	
	/**
	 * Creates a new {@link SimpleHashtable} with DEFAULT_HASH_SLOTS slots.
	 */
	public SimpleHashtable() {
		this(DEFAULT_HASH_SLOTS);
	}
	
	/**
	 * Creates a new {@link SimpleHashtable} with specified number of slots.
	 * @param numSlots Number of slots to use (rounded up to be a number which is a power of 2)
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashtable(int numSlots) {
		int slotCount = 1;
		while (slotCount < numSlots) {
			slotCount <<= 1;
		}
		table = (TableEntry<K, V>[])new TableEntry[slotCount];
		size = 0;
	}
	
	/**
	 * Connects the key to a new value. If key already exists, its value will be changed. If key is null, nothing will be done.
	 * @param key Key to which the value will be assigned to.
	 * @param value A value which will be assigned to the key.
	 */
	public void put(K key, V value) {
		
		if (key == null) {
			return;
		}
		
		int index = getIndex(key);
		TableEntry<K, V> entry = table[index];
		TableEntry<K, V> prevEntry = null;
		
		while (entry != null) {
			if (entry.getKey().equals(key)) {
				entry.setValue(value);
				return;
			}
			prevEntry = entry;
			entry = entry.getNext();
		}
		
		TableEntry<K, V> newEntry = new TableEntry<K, V>(key, value);
		
		if (prevEntry == null) {
			table[index] = newEntry;
		} else {
			prevEntry.setNext(newEntry);
		}
		
		size++;
		
	}
	
	/**
	 * Searches for key in the hash table. If the key is found, its value is returned. If it is not found, null will be returned.
	 * @param key Key whose value will be returned.
	 * @return Value of the key or null if the key doesn't exist in the hash table.
	 */
	public V get(K key) {
		
		if (key == null) {
			return null;
		}
		
		int index = getIndex(key);
		TableEntry<K, V> entry = table[index];
		
		while (entry != null) {
			if (entry.getKey().equals(key)) {
				return entry.getValue();
			}
			entry = entry.getNext();
		}
		
		return null;
		
	}
	
	/**
	 * Returns the number of elements currently in this hash table.
	 * @return Number of elements in the hash table.
	 */
	public int size() {
		return size;
	}
	
	/**
	 * Checks whether this hash table contains a specific key.
	 * @param key Key which will be looked for in this hash table.
	 * @return If the key is found, true is returned, while false is returned otherwise.
	 */
	public boolean containsKey(K key) {
		
		if (key == null) {
			return false;
		}
		
		int index = getIndex(key);
		TableEntry<K, V> entry = table[index];
		
		while (entry != null) {
			if (entry.getKey().equals(key)) {
				return true;
			}
			entry = entry.getNext();
		}
		
		return false;
		
	}
	
	/**
	 * Checks wheter this hash table contains a specific value.
	 * @param value Value which will be looked for in this hash table.
	 * @return If the key with the given value is found, true is returned, while false is returned otherwise.
	 */
	public boolean containsValue(V value) {
		
		for (TableEntry<K, V> entrySlot : table) {
			
			TableEntry<K, V> entry = entrySlot;
			
			while (entry != null) {
				if (entry.getValue().equals(value)) {
					return true;
				}
				entry = entry.getNext();
			}
			
		}
		
		return false;
		
	}
	
	/**
	 * Removes a specific key from the hash table.
	 * @param key Key which, if found, will be removed from the table.
	 */
	public void remove(K key) {
		
		if (key == null) {
			return;
		}
		
		int index = getIndex(key);
		TableEntry<K, V> entry = table[index];
		TableEntry<K, V> prevEntry = null;
		
		while (entry != null) {
			
			if (entry.getKey().equals(key)) {
				
				if (prevEntry == null) {
					table[index] = entry.getNext();
				} else {
					prevEntry.setNext(entry.getNext());
				}
				
				size--;
				
				return;
				
			}
			
			prevEntry = entry;
			entry = entry.getNext();
			
		}
		
	}
	
	/**
	 * Checks whether the hash table is empty (its size is equal to 0).
	 * @return If the table is empty, true is returned, while false is returned otherwise.
	 */
	public boolean isEmpty() {
		return (size == 0);
	}
	
	private int getIndex(K key) {
		return ((key.hashCode() % table.length) + table.length) % table.length;
	}
	
	@Override
	public String toString() {
		return "SimpleHashtable with " + Integer.toString(table.length) + " slots and " + Integer.toString(size) + ((size == 1) ? "entry." : " entries.");
	}
	
}
