package hr.fer.oop.lab4.third;

import java.io.FileInputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Main (entry) class of task three.
 * @author Anthony
 * @version 1.0.0.0
 */
public abstract class Program {

	private Program() { }
	
	/**
	 * Main (entry) method of task three.
	 * @param args Array of arguments passed via the command-line or terminal to the process.
	 */
	public static void main(String[] args) {
		
		Path inputPath = Paths.get("./input.txt");
		Path encryptedFile = Paths.get("./encrypted.dat");
		Path decryptedFile = Paths.get("./decrypted.txt");
		
		byte key = 0x69;
		
		cryptFile(inputPath, encryptedFile, key);
		cryptFile(encryptedFile, decryptedFile, key);
		
		System.out.println("Program complete!");
		
	}
	
	public static void cryptFile(Path inputPath, Path outputPath, byte key) {
		
		FileInputStream fileInputStream = null;
		
		try {
			
			fileInputStream = new FileInputStream(inputPath.toFile());
			(new MyCriptByteWriter(fileInputStream, outputPath, key)).run();	
			
		} catch (Exception ex) {
			
			System.err.println("Program failed (" + ex.getMessage() + ")");
			System.exit(-1);
			
		} finally {
			
			try {
				fileInputStream.close();
			} catch (Exception ex) {
				// Unable to close fileInputStream
			}
			
		}
		
	}
	
}
