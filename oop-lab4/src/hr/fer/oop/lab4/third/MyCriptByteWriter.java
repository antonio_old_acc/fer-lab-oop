package hr.fer.oop.lab4.third;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

import hr.fer.oop.lab4.first.MyByteWriter;

/**
 * Cryptographic byte writer reads an entire input stream and writes read data to a file, encrypting it with an 8-bit encryption key.
 * @author Anthony
 * @version 1.0.0.0
 */
public class MyCriptByteWriter extends MyByteWriter {

	private byte key;
	
	/**
	 * Creates a new cryptographic byte writer object.
	 * @param inputStream Input stream from which the data will be read.
	 * @param path Path to a file to which the data will be written.
	 * @param key Key which will be used to encrypt the data.
	 */
	public MyCriptByteWriter(InputStream inputStream, Path path, byte key) {
		super(inputStream, path);
		this.key = key;
	}

	/**
	 * Gets the encryption key.
	 * @return Encryption key.
	 */
	public byte getKey() {
		return key;
	}

	@Override
	protected OutputStream openFileOutputStream(Path path) throws FileNotFoundException {
		return new MaskStream(new FileOutputStream(path.toFile(), false), key);
	}
	
}
