package hr.fer.oop.lab4.third;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Test {
	
	@org.junit.Test
	public void testFileSizes() {
		
		Path inputPath = Paths.get("./input.txt");
		Path encryptedFile = Paths.get("./encrypted.dat");
		Path decryptedFile = Paths.get("./decrypted.txt");
		
		assertTrue(inputPath.toFile().exists());
		assertTrue(encryptedFile.toFile().exists());
		assertTrue(decryptedFile.toFile().exists());
		
		assertEquals(inputPath.toFile().length(), encryptedFile.toFile().length());
		assertEquals(inputPath.toFile().length(), decryptedFile.toFile().length());
		
	}
	
	@org.junit.Test
	public void testFileEquality() throws IOException {
		
		Path inputPath = Paths.get("./input.txt");
		Path decryptedFile = Paths.get("./decrypted.txt");
		
		byte[] input = Files.readAllBytes(inputPath);
		byte[] output = Files.readAllBytes(decryptedFile);
		
		assertEquals(input.length, output.length);
		for (int i = 0; i < input.length; i++) {
			assertEquals(input[i], output[i]);
		}
		
	}

}
