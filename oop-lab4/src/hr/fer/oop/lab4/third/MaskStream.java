package hr.fer.oop.lab4.third;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Mask stream streams data encrypted with an 8-bit key.
 * @author Anthony
 * @version 1.0.0.0
 */
public class MaskStream extends OutputStream {

	private OutputStream outputStream;
	private byte key;
	
	/**
	 * Creates new mask stream.
	 * @param outputStream Stream to where the data will be written.
	 * @param key Key for encrypting the data.
	 */
	public MaskStream(OutputStream outputStream, byte key) {
		this.outputStream = outputStream;
		setKey(key);
	}
	
	/**
	 * Gets the key of encryption.
	 * @return Key of encryption.
	 */
	public byte getKey() {
		return key;
	}

	/**
	 * Sets the key of encryption.
	 * @param key Key of encryption.
	 */
	public void setKey(byte key) {
		this.key = key;
	}
	
	@Override
	public void write(int b) throws IOException {
		outputStream.write(b ^ key);
	}

}
