package hr.fer.oop.lab4.second;

/**
 * Article represents an object that is sold in the store.
 * @author Anthony
 * @version 1.0.0.0
 */
public class Artikl implements Comparable<Artikl> {

	private String naziv;
	private float cijena;
	
	/**
	 * Creates a new article.
	 * @param naziv Name of the article.
	 * @param cijena Price of the article.
	 */
	public Artikl(String naziv, float cijena) {
		this.naziv = naziv;
		this.cijena = cijena;
	}

	/**
	 * Gets the name of the article.
	 * @return Name of the article.
	 */
	public String getNaziv() {
		return naziv;
	}

	/**
	 * Gets the price of the article.
	 * @return Price of the article.
	 */
	public float getCijena() {
		return cijena;
	}
	
	@Override
	public String toString() {
		return naziv;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Artikl) {
			return naziv.equals(((Artikl)obj).naziv);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return naziv.hashCode();
	}

	@Override
	public int compareTo(Artikl o) {
		return naziv.compareTo(o.naziv);
	}
	
}
