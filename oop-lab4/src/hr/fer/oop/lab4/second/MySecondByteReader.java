package hr.fer.oop.lab4.second;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.HashSet;

/**
 * Byte writer reads the content of each receipt in directory 'racuni' and parses all articles, adding them to a collection.
 * @author Anthony
 * @version 1.0.0.0
 */
public class MySecondByteReader implements FileVisitor<Path> {

	private Collection<Artikl> collection;
	
	/**
	 * Creates a new byte reader object.
	 */
	public MySecondByteReader() {
		this.collection = new HashSet<Artikl>();
	}
	
	/**
	 * Reads the content of each receipt in directory 'racuni' and parses all articles, adding them to a collection.
	 * @throws IOException If any of the found files cannot be read from.
	 */
	public void run() throws IOException {
		collection.clear();
		Files.walkFileTree(Paths.get("./racuni"), this);
	}
	
	/**
	 * Gets the collection of all articles found in the receipts.
	 * @return Collection of all articles found in the receipts.
	 */
	public Collection<Artikl> getCollection() {
		return collection;
	}

	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		
		if (file.toString().endsWith(".txt")) {
			
			System.out.println("Reading file '" + file.toString() + "'.");
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.toFile())));
			
			for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
				
				String trimmedLine = line.trim();
				
				if ((!trimmedLine.startsWith("Proizvod")) && (!trimmedLine.startsWith("UKUPNO")) && (!trimmedLine.startsWith("Račun")) && (!trimmedLine.startsWith("Kupac")) && (!trimmedLine.startsWith("---") && (trimmedLine.length() >= 90))) {
					
					String nameSegment = line.substring(0, 80).trim();
					String priceSegment = line.substring(80, 90).trim();
					
					try {
						
						Artikl artikl = new Artikl(nameSegment, Float.parseFloat(priceSegment));
						if (collection.add(artikl)) {
							System.out.println("Found new article '" + nameSegment + "'.");
						}
						
					} catch (Exception ex) {
						
						System.err.println("Invalid price for article '" + nameSegment + "'.");
						
					}
					
				}
				
			}
			
			bufferedReader.close();
			
		} else {
			
			System.out.println("Skipping file '" + file.toString() + "'.");
			
		}
		
		return FileVisitResult.CONTINUE;
		
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		return FileVisitResult.CONTINUE;
	}
	
}
