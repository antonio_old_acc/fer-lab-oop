package hr.fer.oop.lab4.second;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Set;
import java.util.TreeSet;

/**
 * Main (entry) class of task two.
 * @author Anthony
 * @version 1.0.0.0
 */
public abstract class Program {

	private Program() { }
	
	/**
	 * Main (entry) method of task two.
	 * @param args Array of arguments passed via the command-line or terminal to the process.
	 */
	public static void main(String[] args) {
		
		MySecondByteReader mySecondByteReader = new MySecondByteReader();
		
		try {
			mySecondByteReader.run();
		} catch (IOException ex) {
			System.err.println("Program failed (" + ex.getMessage() + ")");
			System.exit(-1);
		}
		
		Set<Artikl> articleSet = new TreeSet<Artikl>(mySecondByteReader.getCollection());
		
		BufferedWriter bufferedWriter1 = null, bufferedWriter2 = null;
		
		try {
			
			bufferedWriter1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("./cjenik.88592.txt", false), "ISO-8859-2"));
			bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("./cjenik.utf8.txt", false), "UTF-8"));
			
			for (Artikl article : articleSet) {
				bufferedWriter1.write(article.getNaziv() + System.lineSeparator());
				bufferedWriter2.write(article.getNaziv() + System.lineSeparator());
			}
			
			
		} catch (IOException ex) {
			
			System.err.println("Program failed (" + ex.getMessage() + ")");
			System.exit(-1);
			
		} finally {
			
			try {
				bufferedWriter1.close();
			} catch (Exception ex) {
				// Unable to close bufferedWriter1
			}
			
			try {
				bufferedWriter2.close();
			} catch (Exception ex) {
				// Unable to close bufferedWriter2
			}
			
		}
		
		System.out.println("Program complete!");
		
	}
	
}
