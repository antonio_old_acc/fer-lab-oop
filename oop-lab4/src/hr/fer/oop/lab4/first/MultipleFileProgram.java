package hr.fer.oop.lab4.first;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Main (entry) class for the second part of task one.
 * @author Anthony
 * @version 1.0.0.0
 */
public abstract class MultipleFileProgram {

	private MultipleFileProgram() { }
	
	/**
	 * Main (entry) method for the second part of task one.
	 * @param args Array of arguments passed via the command-line or terminal to the process.
	 */
	public static void main(String[] args) {
		
		Path outputPath = Paths.get("./multiout.txt");
		
		if (!outputPath.toFile().exists()) {
			try {
				outputPath.toFile().createNewFile();
			} catch (IOException ex) {
				System.err.println("Output file '" + outputPath.toString() + "' could not be created.");
				System.exit(-1);
			}
		}
		
		FileOutputStream fileOutputStream = null;
		
		try {
			
			fileOutputStream = new FileOutputStream(outputPath.toFile(), false);
			(new MyByteReader(fileOutputStream)).run();
			
		} catch (IOException ex) {
			
			System.err.println("Program failed (" + ex.getMessage() + ")");
			System.exit(-1);
			
		} finally {
			
			try {
				fileOutputStream.close();
			} catch (IOException e) {
				// Unable to close fileOutputStream
			}
			
		}
		
		System.out.println("Program complete!");
	}

}
