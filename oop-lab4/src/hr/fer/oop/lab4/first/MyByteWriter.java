package hr.fer.oop.lab4.first;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

/**
 * Byte writer reads an entire input stream and writes read data to a file.
 * @author Anthony
 * @version 1.0.0.0
 */
public class MyByteWriter {
	
	private static final int BUFFER_SIZE = 32;
	
	private BufferedInputStream bufferedInputStream;
	private Path path;
	
	/**
	 * Creates a new byte writer object.
	 * @param inputStream Input stream from which the data will be read.
	 * @param path Path to a file to which the data will be written.
	 */
	public MyByteWriter(InputStream inputStream, Path path) {
		this.bufferedInputStream = new BufferedInputStream(inputStream);
		this.path = path;
	}
	
	/**
	 * Reads the entire input stream and writes read data to the file.
	 * @throws IOException If the input stream cannot be read from, the output file is invalid or cannot be written to.
	 */
	public void run() throws IOException {
		
		OutputStream outputStream = null;
		
		try {
			
			outputStream = openFileOutputStream(path);
			byte[] buffer = new byte[BUFFER_SIZE];
			
			for (int length = bufferedInputStream.read(buffer); length != -1; length = bufferedInputStream.read(buffer)) {
				outputStream.write(buffer, 0, length);
			}
			
		} finally {
			
			outputStream.close();
			
		}
		
	}
	
	/**
	 * Creates a new output stream file.
	 * @param path Path to file.
	 * @return New output stream for file.
	 * @throws FileNotFoundException If the output file is invalid or cannot be written to.
	 */
	protected OutputStream openFileOutputStream(Path path) throws FileNotFoundException {
		return new FileOutputStream(path.toFile(), false);
	}
	
}
