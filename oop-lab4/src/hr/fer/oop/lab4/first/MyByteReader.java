package hr.fer.oop.lab4.first;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Byte writer reads the content of each receipt in directory 'racuni' and writes it to an output stream.
 * @author Anthony
 * @version 1.0.0.0
 */
public class MyByteReader implements FileVisitor<Path> {

	private static final int BUFFER_SIZE = 32;
	
	private BufferedOutputStream bufferedOutputStream;
	
	/**
	 * Creates a new byte reader object.
	 * @param outputStream Output stream to which the content of each receipt will be written.
	 */
	public MyByteReader(OutputStream outputStream) {
		this.bufferedOutputStream = new BufferedOutputStream(outputStream);
	}
	
	/**
	 * Reads the content of each receipt in directory 'racuni' and writes it to the output stream.
	 * @throws IOException If the output stream cannot be written to or any of the found files cannot be read from.
	 */
	public void run() throws IOException {
		Files.walkFileTree(Paths.get("./racuni"), this);
	}
	
	@Override
	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		
		if (file.toString().endsWith(".txt")) {
			
			System.out.println("Reading file '" + file.toString() + "'.");
			
			BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file.toFile()));
			byte[] buffer = new byte[BUFFER_SIZE];
			
			for (int length = bufferedInputStream.read(buffer); length != -1; length = bufferedInputStream.read(buffer)) {
				bufferedOutputStream.write(buffer, 0, length);
			}
			
			bufferedInputStream.close();
			
		} else {
			
			System.out.println("Skipping file '" + file.toString() + "'.");
			
		}
		
		return FileVisitResult.CONTINUE;
	
	}

	@Override
	public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
		return FileVisitResult.CONTINUE;
	}

	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		return FileVisitResult.CONTINUE;
	}
	
	
}
