package hr.fer.oop.lab4.first;

import java.io.FileInputStream;
import java.nio.file.Paths;

/**
 * Main (entry) class for the first part of task one.
 * @author Anthony
 * @version 1.0.0.0
 */
public abstract class SingleFileProgram {

	private SingleFileProgram() { }
	
	/**
	 * Main (entry) method for the first part of task one.
	 * @param args Array of arguments passed via the command-line or terminal to the process.
	 */
	public static void main(String[] args) {
		
		FileInputStream fileInputStream = null;
		
		try {
			
			fileInputStream = new FileInputStream("./racuni/2015/1/Racun_0.txt");
			(new MyByteWriter(fileInputStream, Paths.get("./singleout.txt"))).run();
			
		} catch (Exception ex) {
			
			System.err.println("Program failed (" + ex.getMessage() + ")");
			System.exit(-1);
			
		} finally {
			
			try {
				fileInputStream.close();
			} catch (Exception ex) {
				// Unable to close fileInputStream
			}
			
		}
		
		System.out.println("Program complete!");
		
	}

}
