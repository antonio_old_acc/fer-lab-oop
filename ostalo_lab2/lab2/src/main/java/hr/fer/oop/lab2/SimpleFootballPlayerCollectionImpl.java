package hr.fer.oop.lab2;

import hr.fer.oop.lab2.welcomepack.SimpleFootballPlayerCollection;

/**
 * Implementation of SimpleFootballPlayerCollection interface.
 * @author Anthony
 */
public class SimpleFootballPlayerCollectionImpl implements SimpleFootballPlayerCollection {
	
	private static final int DEFAULT_MAX_PLAYERS = 32;
	
	protected FootballPlayer[] playerArray;
	protected int emptyPlayerSpace = 0;
	
	/**
	 * Creates a football player collection with default max football players count.
	 */
	public SimpleFootballPlayerCollectionImpl() {
		this(DEFAULT_MAX_PLAYERS);
	}
	
	/**
	 * Creates a football player collection with specified max football players count.
	 * @param maxPlayers Max football players count.
	 */
	public SimpleFootballPlayerCollectionImpl(int maxPlayers) {
		if (maxPlayers <= 0) {
			maxPlayers = DEFAULT_MAX_PLAYERS;
		}
		playerArray = new FootballPlayer[maxPlayers];
	}
	
	@Override
	public boolean add(FootballPlayer player) {
		if (player == null) {
			return false;
		}
		if (emptyPlayerSpace < playerArray.length) {
			for (int i = 0; i < emptyPlayerSpace; i++) {
				if (player.equals(playerArray[i])) {
					return false;
				}
			}
			playerArray[emptyPlayerSpace++] = player;
			return true;
		}
		return false;
	}

	/**
	 * Removes the football player from the collection if he was added to it.
	 * @param player Football player to remove.
	 * @return If player was found, true is returned, while false is returned otherwise.
	 */
	public boolean remove(FootballPlayer player) {
		if (player == null) {
			return false;
		}
		for (int i = 0; i < emptyPlayerSpace; i++) {
			if (player.equals(playerArray[i])) {
				for (int j = i + 1; j < emptyPlayerSpace; j++) {
					playerArray[j - 1] = playerArray[j];
				}
				emptyPlayerSpace--;
				return true;
			}
		}
		return false;
	}

	@Override
	public int calculateEmotionSum() {
		int emotionSum = 0;
		for (int i = 0; i < emptyPlayerSpace; i++) {
			emotionSum += playerArray[i].getEmotion();
		}
		return emotionSum;
	}

	@Override
	public int calculateSkillSum() {
		int skillSum = 0;
		for (int i = 0; i < emptyPlayerSpace; i++) {
			skillSum += playerArray[i].getPlayingSkill();
		}
		return skillSum;
	}

	@Override
	public void clear() {
		for (int i = 0; i < emptyPlayerSpace; i++) {
			playerArray[i] = null;
		}
		emptyPlayerSpace = 0;
	}

	@Override
	public boolean contains(FootballPlayer player) {
		if (player == null) {
			return false;
		}
		for (int i = 0; i < emptyPlayerSpace; i++) {
			if (player.equals(playerArray[i])) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int getMaxSize() {
		return playerArray.length;
	}

	@Override
	public FootballPlayer[] getPlayers() {
		// Sigurno da se neće modificirati array izvana, no vremenski ne-efektivna operacija
		FootballPlayer[] players = new FootballPlayer[emptyPlayerSpace];
		for (int i = 0; i < emptyPlayerSpace; i++) {
			players[i] = playerArray[i];
		}
		return players;
	}

	@Override
	public int size() {
		return emptyPlayerSpace;
	}

}
