package hr.fer.oop.lab2.demo;

import hr.fer.oop.lab2.FootballPlayer;
import hr.fer.oop.lab2.ResizableSimpleFootballPlayerCollectionImpl;
import hr.fer.oop.lab2.SearchUtil;
import hr.fer.oop.lab2.welcomepack.PlayingPosition;
import hr.fer.oop.lab2.welcomepack.SimpleFootballPlayerCollection;

public class Zad2Demo {
	
	private static void printFunc(String posTitle, SimpleFootballPlayerCollection collection) {
		System.out.println("Lista svih " + posTitle + ":");
		for (FootballPlayer p : collection.getPlayers()) {
			System.out.println(p.toString());
		}
		System.out.println();
	}
	
	public static void main(String[] args) {
		
		ResizableSimpleFootballPlayerCollectionImpl collection = new ResizableSimpleFootballPlayerCollectionImpl();
		
		collection.add(new FootballPlayer("Pero", "Croatia", 30, 30, PlayingPosition.DF));
		collection.add(new FootballPlayer("Ivan", "Croatia", 40, 40, PlayingPosition.DF));
		collection.add(new FootballPlayer("Jure", "Croatia", 50, 50, PlayingPosition.MF));
		collection.add(new FootballPlayer("Mirko", "Croatia", 70, 70, PlayingPosition.MF));
		collection.add(new FootballPlayer("Marko", "Croatia", 75, 75, PlayingPosition.GK));
		
		SimpleFootballPlayerCollection aboveMedian = SearchUtil.retrieveAboveMedianSkillPlayers(collection);
		
		System.out.println("Iznad medijana:");
		for (FootballPlayer p : aboveMedian.getPlayers()) {
			System.out.println(p.toString());
		}
		System.out.println();
		
		FootballPlayer lowSkilled = SearchUtil.retrieveLowSkilledPlayer(collection, PlayingPosition.FW);
		
		System.out.println("Najlosiji FW: " + ((lowSkilled == null) ? ("nema") : (lowSkilled.toString())));
		
		lowSkilled = SearchUtil.retrieveLowSkilledPlayer(collection, PlayingPosition.DF);
		
		System.out.println("Najlosiji DF: " + ((lowSkilled == null) ? ("nema") : (lowSkilled.toString())));
		
		lowSkilled = SearchUtil.retrieveLowSkilledPlayer(collection, PlayingPosition.GK);
		
		System.out.println("Najlosiji GK: " + ((lowSkilled == null) ? ("nema") : (lowSkilled.toString())));
		
		System.out.println();
		
		SimpleFootballPlayerCollection allDF = SearchUtil.retrievePlayingPositionPlayers(collection, PlayingPosition.DF);
		SimpleFootballPlayerCollection allMF = SearchUtil.retrievePlayingPositionPlayers(collection, PlayingPosition.MF);
		SimpleFootballPlayerCollection allFW = SearchUtil.retrievePlayingPositionPlayers(collection, PlayingPosition.FW);
		SimpleFootballPlayerCollection allGK = SearchUtil.retrievePlayingPositionPlayers(collection, PlayingPosition.GK);
		
		printFunc("DF", allDF);
		printFunc("MF", allMF);
		printFunc("FW", allFW);
		printFunc("GK", allGK);
		
	}
	
}
