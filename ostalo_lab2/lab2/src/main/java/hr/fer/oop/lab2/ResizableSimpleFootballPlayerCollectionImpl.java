package hr.fer.oop.lab2;

public class ResizableSimpleFootballPlayerCollectionImpl extends SimpleFootballPlayerCollectionImpl {

	public ResizableSimpleFootballPlayerCollectionImpl() {
		super();
	}
	
	public ResizableSimpleFootballPlayerCollectionImpl(int startingCapacity) {
		super(startingCapacity);
	}
	
	@Override
	public boolean add(FootballPlayer player) {
		if (emptyPlayerSpace >= playerArray.length) {
			FootballPlayer[] newArray = new FootballPlayer[2 * playerArray.length];
			for (int i = 0; i < playerArray.length; i++) {
				newArray[i] = playerArray[i];
			}
			playerArray = newArray;
		}
		return super.add(player);
	}
	
	@Override
	public int getMaxSize() { /* Ova vec vraca dobru vrijednost u nadklasi */
		return super.getMaxSize();
	}
	
}
