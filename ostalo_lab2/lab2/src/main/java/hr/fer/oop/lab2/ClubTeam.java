package hr.fer.oop.lab2;

import hr.fer.oop.lab2.welcomepack.Formation;

public class ClubTeam extends Team {

	public static final int REPUTATION_MIN_VALUE = 0;
	public static final int REPUTATION_MAX_VALUE = 100;
	
	private static final int MAX_REGISTERED_PLAYERS = 25;
	private static final int DEFAULT_REPUTATUON = 50;
	private static final double TEAM_RATING_SKILL_RATIO = 0.7;
	private static final double TEAM_RATING_EMOTION_RATIO = 0.3;
	
	private int reputation;
	
	/**
	 * Creates a new club football team with default values.
	 */
	public ClubTeam() {
		super(MAX_REGISTERED_PLAYERS);
		this.reputation = DEFAULT_REPUTATUON;
	}
	
	/**
	 * Creates a new club football team with default values for unspecified attributes.
	 * @param name Club team name.
	 * @param formation Club team formation.
	 * @throws InvalidTeamInitializationException Thrown when invalid parameters are provided.
	 */
	public ClubTeam(String name, Formation formation) throws InvalidTeamInitializationException {
		this(name, formation, DEFAULT_REPUTATUON);
	}
	
	/**
	 * Creates a new club football team.
	 * @param name Club team name.
	 * @param formation Club team formation.
	 * @param reputation National team reputation.
	 * @throws InvalidTeamInitializationException Thrown when invalid parameters are provided.
	 */
	public ClubTeam(String name, Formation formation, int reputation) throws InvalidTeamInitializationException {
		super(MAX_REGISTERED_PLAYERS, name, formation);
		if (!setReputation(reputation)) {
			// throw
			new InvalidTeamInitializationException(String.format("Parameter 'reputation' must be a value between %d and %d (both inclusive).", REPUTATION_MIN_VALUE, REPUTATION_MAX_VALUE));
			this.reputation = DEFAULT_REPUTATUON;
		}
	}

	/**
	 * Gets the club team reputation.
	 * @return Club team reputation.
	 */
	public int getReputation() {
		return reputation;
	}

	/**
	 * Sets the current team reputation. The value must be between REPUTATION_MIN_VALUE and REPUTATION_MAX_VALUE (both inclusive).
	 * @param reputation Team reputation.
	 * @return On success, true is returned, while false is returned on failure.
	 */
	public boolean setReputation(int reputation) {
		if ((reputation >= REPUTATION_MIN_VALUE) && (reputation <= REPUTATION_MAX_VALUE)) {
			this.reputation = reputation;
			return true;
		}
		return false;
	}

	@Override
	public boolean registerPlayer(FootballPlayer player) {
		if (player.getPlayingSkill() >= this.reputation) {
			return super.registerPlayer(player);
		}
		return false;
	}
	
	@Override
	public double calculateRating() {
		return TEAM_RATING_SKILL_RATIO * getRegisteredPlayers().calculateSkillSum() + TEAM_RATING_EMOTION_RATIO * getRegisteredPlayers().calculateEmotionSum();
	}
	
}
