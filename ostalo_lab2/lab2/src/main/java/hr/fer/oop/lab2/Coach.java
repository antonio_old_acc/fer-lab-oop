package hr.fer.oop.lab2;

import hr.fer.oop.lab2.welcomepack.Formation;
import hr.fer.oop.lab2.welcomepack.ManageableTeam;
import hr.fer.oop.lab2.welcomepack.Manager;
import hr.fer.oop.lab2.welcomepack.SimpleFootballPlayerCollection;

/**
 * An representation of a coach.
 * @author Anthony
 */
public class Coach extends Person implements Manager {
	
	public static final int COACHING_SKILL_MIN_VALUE = 0;
	public static final int COACHING_SKILL_MAX_VALUE = 100;

	private static final int DEFAULT_COACHING_SKILL = 50;
	private static final Formation DEFAULT_FORMATION = Formation.F442;
	
	private int coachingSkill;
	private Formation formation;
	private ManageableTeam managableTeam;
	
	/**
	 * Creates a new coach with default values.
	 */
	public Coach() {
		super();
		coachingSkill = DEFAULT_COACHING_SKILL;
		formation = DEFAULT_FORMATION;
	}
	
	/**
	 * Creates a new coach with default values for unspecified attributes.
	 * @param name Full name of the newly created coach.
	 * @param country Country of origin of the newly created coach.
	 * @param emotion Current emotion of the newly created coach.
	 * @throws InvalidPersonInitializationException Thrown when invalid parameters are provided.
	 */
	public Coach(String name, String country, int emotion) throws InvalidPersonInitializationException {
		this(name, country, emotion, DEFAULT_COACHING_SKILL);
	}
	
	/**
	 * Creates a new coach with default values for unspecified attributes.
	 * @param name Full name of the newly created coach.
	 * @param country Country of origin of the newly created coach.
	 * @param emotion Current emotion of the newly created coach.
	 * @param coachingSkill Current coaching skill of the newly created coach.
	 * @throws InvalidPersonInitializationException Thrown when invalid parameters are provided.
	 */
	public Coach(String name, String country, int emotion, int coachingSkill) throws InvalidPersonInitializationException {
		this(name, country, emotion, coachingSkill, DEFAULT_FORMATION);
	}
	
	/**
	 * Creates a new coach.
	 * @param name Full name of the newly created coach.
	 * @param country Country of origin of the newly created coach.
	 * @param emotion Current emotion of the newly created coach.
	 * @param coachingSkill Current coaching skill of the newly created coach.
	 * @param formation Favorite formation of the newly created coach.
	 * @throws InvalidPersonInitializationException Thrown when invalid parameters are provided.
	 */
	public Coach(String name, String country, int emotion, int coachingSkill, Formation formation) throws InvalidPersonInitializationException {
		super(name, country, emotion);
		if (!setCoachingSkill(coachingSkill)) {
			// throw
			new InvalidPersonInitializationException(String.format("Parameter 'coachingSkill' must be a value between %d and %d (both inclusive).", COACHING_SKILL_MIN_VALUE, COACHING_SKILL_MAX_VALUE));
			this.coachingSkill = DEFAULT_COACHING_SKILL;
		}
		if (!setFormation(formation)) {
			// throw
			new InvalidPersonInitializationException("Parameter 'formation' cannot be null.");
			this.formation = DEFAULT_FORMATION;
		}
	}
	
	/**
	 * Gets current coaching skill of the coach.
	 * @return Current coaching skill of the coach.
	 */
	public int getCoachingSkill() {
		return coachingSkill;
	}
	
	/**
	 * Sets current coaching skill of the coach. The value must be between COACHING_SKILL_MIN_VALUE and COACHING_SKILL_MAX_VALUE (both inclusive).
	 * @param coachingSkill Current coaching skill of the coach.
	 * @return On success, true is returned, while false is returned on failure.
	 */
	public boolean setCoachingSkill(int coachingSkill) {
		if ((coachingSkill >= COACHING_SKILL_MIN_VALUE) && (coachingSkill <= COACHING_SKILL_MAX_VALUE)) {
			this.coachingSkill = coachingSkill;
			return true;
		}
		return false;
	}

	/**
	 * Gets favorite formation of the coach.
	 * @return Favorite formation of the coach.
	 */
	public Formation getFormation() {
		return formation;
	}

	/**
	 * Sets favorite formation of the coach.
	 * @param formation Favorite formation of the coach.
	 * @return On success, true is returned, while false is returned on failure.
	 */
	public boolean setFormation(Formation formation) {
		if (formation != null) {
			this.formation = formation;
			return true;
		}
		return false;
	}

	@Override
	public void pickStartingEleven() {
		SimpleFootballPlayerCollection collection = managableTeam.getRegisteredPlayers();
		Formation teamFormation = managableTeam.getFormation();
		
		FootballPlayer gk = null;
		SimpleFootballPlayerCollectionImpl dfList;
		SimpleFootballPlayerCollectionImpl mfList;
		SimpleFootballPlayerCollectionImpl fwList;
		
		dfList = new SimpleFootballPlayerCollectionImpl(teamFormation.getNoDF());
		mfList = new SimpleFootballPlayerCollectionImpl(teamFormation.getNoMF());
		fwList = new SimpleFootballPlayerCollectionImpl(teamFormation.getNoFW());
		
		for (FootballPlayer player : collection.getPlayers()) {
			SimpleFootballPlayerCollectionImpl observedCollection;
			switch (player.getPlayingPosition()) {
			case GK:
				if ((gk == null) || (comparePlayers(player, gk))) {
					gk = player;
				}
				continue;
			case DF:
				observedCollection = dfList;
				break;
			case MF:
				observedCollection = mfList;
				break;
			case FW:
				observedCollection = fwList;
				break;
			default:
				continue; // To suppress some warnings
			}
			if ((observedCollection.size() < observedCollection.getMaxSize())) {
				observedCollection.add(player);
			} else {
				FootballPlayer leastSkilled = null;
				for (FootballPlayer observedPlayer : observedCollection.getPlayers()) {
					if ((leastSkilled == null) || (observedPlayer.getPlayingSkill() < leastSkilled.getPlayingSkill())) {
						leastSkilled = observedPlayer;
					}
				}
				if (player.getPlayingSkill() > leastSkilled.getPlayingSkill()) {
					observedCollection.remove(leastSkilled);
					observedCollection.add(player);
				}
			}
		}
		
		managableTeam.clearStartingEleven();
		managableTeam.addPlayerToStartingEleven(gk);
		for (FootballPlayer fw : fwList.getPlayers()) {
			managableTeam.addPlayerToStartingEleven(fw);
		}
		for (FootballPlayer mf : mfList.getPlayers()) {
			managableTeam.addPlayerToStartingEleven(mf);
		}
		for (FootballPlayer df : dfList.getPlayers()) {
			managableTeam.addPlayerToStartingEleven(df);
		}
	}
	
	/**
	 * Compares two football players to determine which one is more suitable for the game.
	 * @param first The first football player.
	 * @param second The second football player.
	 * @return If the first one is more suitable than the second one, true is returned, while false is returned otherwise.
	 */
	private boolean comparePlayers(FootballPlayer first, FootballPlayer second) {
		return (first.getPlayingSkill() >= second.getPlayingSkill());
	}

	@Override
	public void forceMyFormation() {
		managableTeam.setFormation(this.formation);
		
	}

	@Override
	public void setManagingTeam(ManageableTeam team) {
		if (team != null) {
			managableTeam = team;
		}
	}
	
}
