package hr.fer.oop.lab2;

/**
 * Exception that is thrown when a derivative of a Team with invalid parameters is constructed.
 * @author Anthony
 */
public class InvalidTeamInitializationException extends RuntimeException {

	private static final long serialVersionUID = 5618805805087938970L;
	
	/**
	 * Creates an exception.
	 * @param message Message that will be given when this exception is thrown.
	 */
	public InvalidTeamInitializationException(String message) {
		super(message);
		System.err.println(message); // Ovo je trazeno u zadatku, no s obzirom da je u pitanju iznimka, nije nuzan ispis na stderr.
	}

}
