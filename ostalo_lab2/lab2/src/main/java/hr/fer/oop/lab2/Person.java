package hr.fer.oop.lab2;

/**
 * An abstract representation of a person.
 * @author Anthony
 */
public abstract class Person {
	
	public static final int EMOTION_MIN_VALUE = 0;
	public static final int EMOTION_MAX_VALUE = 100;
	
	private static final String DEFAULT_NAME = "John Doe";
	private static final String DEFAULT_COUNTRY = "Noland";
	private static final int DEFAULT_EMOTION = 50;
	
	private String name;
	private String country;
	private int emotion;
	
	/**
	 * Creates a new person with default values.
	 */
	protected Person() {
		this(DEFAULT_NAME);
	}
	
	/**
	 * Creates a new person with default values for unspecified attributes.
	 * @param name Full name of the newly created person.
	 * @throws InvalidPersonInitializationException Thrown when invalid parameters are provided.
	 */
	protected Person(String name) throws InvalidPersonInitializationException {
		this(name, DEFAULT_COUNTRY);
	}
	
	/**
	 * Creates a new person with default values for unspecified attributes.
	 * @param name Full name of the newly created person.
	 * @param country Country of origin of the newly created person.
	 * @throws InvalidPersonInitializationException Thrown when invalid parameters are provided.
	 */
	protected Person(String name, String country) throws InvalidPersonInitializationException {
		this(name, country, DEFAULT_EMOTION);
	}
	
	/**
	 * Creates a new person.
	 * @param name Full name of the newly created person.
	 * @param country Country of origin of the newly created person.
	 * @param emotion Current emotion of the newly created person.
	 * @throws InvalidPersonInitializationException Thrown when invalid parameters are provided.
	 */
	protected Person(String name, String country, int emotion) throws InvalidPersonInitializationException {
		if (name == null) {
			// throw
			new InvalidPersonInitializationException("Parameter 'name' cannot be null.");
			this.name = DEFAULT_NAME;
		} else {
			this.name = name;
		}
		if (country == null) {
			// throw
			new InvalidPersonInitializationException("Parameter 'country' cannot be null.");
			this.country = DEFAULT_COUNTRY;
		} else {
			this.country = country;
		}
		if (!setEmotion(emotion)) {
			// throw
			new InvalidPersonInitializationException(String.format("Parameter 'emotion' must be a value between %d and %d (both inclusive).", EMOTION_MIN_VALUE, EMOTION_MAX_VALUE));
			this.emotion = DEFAULT_EMOTION;
		}
	}

	/**
	 * Gets full name of the person.
	 * @return Full name of the person.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets country of origin of the person.
	 * @return Country of origin of the person.
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Gets current emotion of the person.
	 * @return Current emotion of the person.
	 */
	public int getEmotion() {
		return emotion;
	}

	/**
	 * Sets current emotion of the person. The value must be between EMOTION_MIN_VALUE and EMOTION_MAX_VALUE (both inclusive).
	 * @param emotion Current emotion of the person.
	 * @return On success, true is returned, while false is returned on failure.
	 */
	public boolean setEmotion(int emotion) {
		if ((emotion >= EMOTION_MIN_VALUE) && (emotion <= EMOTION_MAX_VALUE)) {
			this.emotion = emotion;
			return true;
		}
		return false;
	}
	
	/**
	 * Represents the current person textually as String. The format is 'name (country)'.
	 * @return Person represented textually as String.
	 */
	@Override
	public String toString() {
		return String.format("%s (%s)", name, country);
	}
	
	/**
	 * Check whether the current person and an another person are the same. Conditions for being the same include having the same full name and same country of origin.
	 * @param obj Another person.
	 * @return If persons are the same, true is returned, while false is returned otherwise.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Person) {
			Person person = (Person)obj;
			return (name.equals(person.name)) && (country.equals(person.country));
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode() ^ country.hashCode();
	}
	
}
