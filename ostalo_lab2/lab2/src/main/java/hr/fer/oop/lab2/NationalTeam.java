package hr.fer.oop.lab2;

import hr.fer.oop.lab2.welcomepack.Formation;

/**
 * An representation of a national football team.
 * @author Anthony
 */
public class NationalTeam extends Team {

	private static final int MAX_REGISTERED_PLAYERS = 23;
	private static final String DEFAULT_COUNTRY = "Noland";
	private static final double TEAM_RATING_SKILL_RATIO = 0.3;
	private static final double TEAM_RATING_EMOTION_RATIO = 0.7;
	
	private String country;

	/**
	 * Creates a new national football team with default values.
	 */
	public NationalTeam() {
		super(MAX_REGISTERED_PLAYERS);
		country = DEFAULT_COUNTRY;
	}
	
	/**
	 * Creates a new national football team with default values for unspecified attributes.
	 * @param name National team name.
	 * @param formation National team formation.
	 * @throws InvalidTeamInitializationException Thrown when invalid parameters are provided.
	 */
	public NationalTeam(String name, Formation formation) throws InvalidTeamInitializationException {
		this(name, formation, DEFAULT_COUNTRY);
	}
	
	/**
	 * Creates a new national football team.
	 * @param name National team name.
	 * @param formation National team formation.
	 * @param country National team country.
	 * @throws InvalidTeamInitializationException Thrown when invalid parameters are provided.
	 */
	public NationalTeam(String name, Formation formation, String country) throws InvalidTeamInitializationException {
		super(MAX_REGISTERED_PLAYERS, name, formation);
		if (country == null) {
			// throw
			new InvalidPersonInitializationException("Parameter 'country' cannot be null.");
			this.country = DEFAULT_COUNTRY;
		} else {
			this.country = country;
		}
	}

	/**
	 * Gets the national team country.
	 * @return National team country.
	 */
	public String getCountry() {
		return country;
	}

	@Override
	public boolean registerPlayer(FootballPlayer player) {
		if (country.equals(player.getCountry())) {
			return super.registerPlayer(player);
		}
		return false;
	}
	
	@Override
	public double calculateRating() {
		return TEAM_RATING_SKILL_RATIO * getRegisteredPlayers().calculateSkillSum() + TEAM_RATING_EMOTION_RATIO * getRegisteredPlayers().calculateEmotionSum();
	}
	
}
