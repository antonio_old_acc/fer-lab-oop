package hr.fer.oop.lab2;

public class Zad1Demo {
	
	public static void main(String[] args) {
		ResizableSimpleFootballPlayerCollectionImpl collection = new ResizableSimpleFootballPlayerCollectionImpl(1);
		
		System.out.println("Max size: " + Integer.toString(collection.getMaxSize()) + ", Current size: " + Integer.toString(collection.size()));
		
		collection.add(new FootballPlayer("Pero", "Croatia", 50));
		
		System.out.println("Max size: " + Integer.toString(collection.getMaxSize()) + ", Current size: " + Integer.toString(collection.size()));
		
		collection.add(new FootballPlayer("Ivan", "Croatia", 50));
		
		System.out.println("Max size: " + Integer.toString(collection.getMaxSize()) + ", Current size: " + Integer.toString(collection.size()));
		
		collection.add(new FootballPlayer("Jure", "Croatia", 50));
		
		System.out.println("Max size: " + Integer.toString(collection.getMaxSize()) + ", Current size: " + Integer.toString(collection.size()));
		
		collection.add(new FootballPlayer("Marko", "Croatia", 50));
		
		System.out.println("Max size: " + Integer.toString(collection.getMaxSize()) + ", Current size: " + Integer.toString(collection.size()));
		
		collection.add(new FootballPlayer("Pero 2", "Croatia", 50));
		
		System.out.println("Max size: " + Integer.toString(collection.getMaxSize()) + ", Current size: " + Integer.toString(collection.size()));
		
	}
	
}
