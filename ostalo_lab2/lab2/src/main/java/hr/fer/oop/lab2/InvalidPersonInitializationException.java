package hr.fer.oop.lab2;

/**
 * Exception that is thrown when a derivative of a Person with invalid parameters is constructed.
 * @author Anthony
 */
public class InvalidPersonInitializationException extends RuntimeException {

	private static final long serialVersionUID = -2524259947896520978L;

	/**
	 * Creates an exception.
	 * @param message Message that will be given when this exception is thrown.
	 */
	public InvalidPersonInitializationException(String message) {
		super(message);
		System.err.println(message); // Ovo je trazeno u zadatku, no s obzirom da je u pitanju iznimka, nije nuzan ispis na stderr.
	}
	
}
