package hr.fer.oop.lab2;

import hr.fer.oop.lab2.welcomepack.PlayingPosition;

/**
 * An representation of a football player.
 * @author Anthony
 */
public class FootballPlayer extends Person {

	public static final int PLAYING_SKILL_MIN_VALUE = 0;
	public static final int PLAYING_SKILL_MAX_VALUE = 100;
	
	private static final int DEFAULT_PLAYING_SKILL = 50;
	private static final PlayingPosition DEFAULT_PLAYING_POSITION = PlayingPosition.MF;
	
	private int playingSkill;
	private PlayingPosition playingPosition;
	
	/**
	 * Creates a new football player with default values.
	 */
	public FootballPlayer() {
		super();
		playingSkill = DEFAULT_PLAYING_SKILL;
		playingPosition = DEFAULT_PLAYING_POSITION;
	}
	
	/**
	 * Creates a new football player with default values for unspecified attributes.
	 * @param name Full name of the newly created football player.
	 * @param country Country of origin of the newly created football player.
	 * @param emotion Current emotion of the newly created football player.
	 * @throws InvalidPersonInitializationException Thrown when invalid parameters are provided.
	 */
	public FootballPlayer(String name, String country, int emotion) throws InvalidPersonInitializationException {
		this(name, country, emotion, DEFAULT_PLAYING_SKILL);
	}
	
	/**
	 * Creates a new football player with default values for unspecified attributes.
	 * @param name Full name of the newly created football player.
	 * @param country Country of origin of the newly created football player.
	 * @param emotion Current emotion of the newly created football player.
	 * @param playingSkill Current playing skill of the newly created football player.
	 * @throws InvalidPersonInitializationException Thrown when invalid parameters are provided.
	 */
	public FootballPlayer(String name, String country, int emotion, int playingSkill) throws InvalidPersonInitializationException {
		this(name, country, emotion, playingSkill, DEFAULT_PLAYING_POSITION);
	}
	
	/**
	 * Creates a new football player.
	 * @param name Full name of the newly created football player.
	 * @param country Country of origin of the newly created football player.
	 * @param emotion Current emotion of the newly created football player.
	 * @param playingSkill Current playing skill of the newly created football player.
	 * @param playingPosition Playing position of the newly created football player.
	 * @throws InvalidPersonInitializationException Thrown when invalid parameters are provided.
	 */
	public FootballPlayer(String name, String country, int emotion, int playingSkill, PlayingPosition playingPosition) throws InvalidPersonInitializationException {
		super(name, country, emotion);
		if (!setPlayingSkill(playingSkill)) {
			//throw
			new InvalidPersonInitializationException(String.format("Parameter 'playingSkill' must be a value between %d and %d (both inclusive).", PLAYING_SKILL_MIN_VALUE, PLAYING_SKILL_MAX_VALUE));
			this.playingSkill = DEFAULT_PLAYING_SKILL;
		}
		if (!setPlayingPosition(playingPosition)) {
			//throw
			new InvalidPersonInitializationException("Parameter 'playingSkill' cannot be null.");
			this.playingPosition = DEFAULT_PLAYING_POSITION;
		}
	}
	
	
	/**
	 * Gets current playing skill of the football player.
	 * @return Current playing skill of the football player.
	 */
	public int getPlayingSkill() {
		return playingSkill;
	}
	
	/**
	 * Sets current playing skill of the football player. The value must be between PLAYING_SKILL_MIN_VALUE and PLAYING_SKILL_MAX_VALUE (both inclusive).
	 * @param playingSkill Current playing skill of the football player.
	 * @return On success, true is returned, while false is returned on failure.
	 */
	public boolean setPlayingSkill(int playingSkill) {
		if ((playingSkill >= PLAYING_SKILL_MIN_VALUE) && (playingSkill <= PLAYING_SKILL_MAX_VALUE)) {
			this.playingSkill = playingSkill;
			return true;
		}
		return false;
	}

	/**
	 * Gets playing position of the football player.
	 * @return Playing position of the football player.
	 */
	public PlayingPosition getPlayingPosition() {
		return playingPosition;
	}

	/**
	 * Sets playing position of the football player.
	 * @param playingPosition Playing position of the football player.
	 * @return On success, true is returned, while false is returned on failure.
	 */
	public boolean setPlayingPosition(PlayingPosition playingPosition) {
		if (playingPosition != null) {
			this.playingPosition = playingPosition;
			return true;
		}
		return false;
	}
	
}
