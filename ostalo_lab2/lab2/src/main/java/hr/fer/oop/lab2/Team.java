package hr.fer.oop.lab2;

import hr.fer.oop.lab2.welcomepack.ManageableTeam;
import hr.fer.oop.lab2.welcomepack.Formation;

/**
 * An abstract class representing a team.
 * @author Anthony
 */
public abstract class Team implements ManageableTeam {
	
	private static final String DEFAULT_TEAM_NAME = "TeamX";
	private static final Formation DEFAULT_TEAM_FORMATION = Formation.F442;
	
	private String name;
	private Formation formation;
	private SimpleFootballPlayerCollectionImpl registeredPlayers;
	private SimpleFootballPlayerCollectionImpl startingEleven = new SimpleFootballPlayerCollectionImpl(11);
	
	/**
	 * Creates a new football team with default values.
	 */
	protected Team(int maxRegisteredPlayers) {
		this(maxRegisteredPlayers, DEFAULT_TEAM_NAME);
	}
	
	/**
	 * Creates a new football team with default values for unspecified attributes.
	 * @param name Football team name.
	 * @throws InvalidTeamInitializationException Thrown when invalid parameters are provided.
	 */
	protected Team(int maxRegisteredPlayers, String name) throws InvalidTeamInitializationException {
		this(maxRegisteredPlayers, name, DEFAULT_TEAM_FORMATION);
	}
	
	/**
	 * Creates a new football team.
	 * @param name Football team name.
	 * @param formation Football team formation.
	 * @throws InvalidTeamInitializationException Thrown when invalid parameters are provided.
	 */
	protected Team(int maxRegisteredPlayers, String name, Formation formation) throws InvalidTeamInitializationException {
		registeredPlayers = new SimpleFootballPlayerCollectionImpl(maxRegisteredPlayers);
		if (name == null) {
			// throw
			new InvalidTeamInitializationException("Parameter 'name' cannot be null.");
			this.name = DEFAULT_TEAM_NAME;
		} else {
			this.name = name;
		}
		if (formation == null) {
			// throw
			new InvalidTeamInitializationException("Parameter 'formation' cannot be null.");
			this.formation = DEFAULT_TEAM_FORMATION;
		} else {
			this.formation = formation;
		}
	}
	
	/**
	 * Gets the name of the football team.
	 * @return Football team name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the formation of the football team.
	 * @return Football team formation.
	 */
	public Formation getFormation() {
		return formation;
	}

	/**
	 * Sets the formation of the football team.
	 * @param formation Football team formation.
	 */
	public void setFormation(Formation formation) {
		if (formation != null) {
			this.formation = formation;
		}
	}
	
	/**
	 * Gets registered players collection.
	 * @return Registered players collection.
	 */
	public SimpleFootballPlayerCollectionImpl getRegisteredPlayers() {
		return registeredPlayers;
	}

	/**
	 * Gets starting eleven players collection.
	 * @return Starting eleven players collection.
	 */
	public SimpleFootballPlayerCollectionImpl getStartingEleven() {
		return startingEleven;
	}

	@Override
	public boolean addPlayerToStartingEleven(FootballPlayer player) {
		if (isPlayerRegistered(player)) {
			return startingEleven.add(player);
		}
		return false;
	}

	@Override
	public boolean isPlayerRegistered(FootballPlayer player) {
		return registeredPlayers.contains(player);
	}

	@Override
	public boolean registerPlayer(FootballPlayer player) {
		return registeredPlayers.add(player);
	}

	@Override
	public void clearStartingEleven() {
		startingEleven.clear();
	}

	@Override
	public abstract double calculateRating();
	
}
