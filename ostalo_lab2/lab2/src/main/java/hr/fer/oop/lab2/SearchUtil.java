package hr.fer.oop.lab2;

import hr.fer.oop.lab2.welcomepack.PlayingPosition;
import hr.fer.oop.lab2.welcomepack.SimpleFootballPlayerCollection;

public class SearchUtil {

	public static SimpleFootballPlayerCollection retrieveAboveMedianSkillPlayers(SimpleFootballPlayerCollection players) {
		int median = calculateMedianValue(players);
		
		SimpleFootballPlayerCollection collection = new ResizableSimpleFootballPlayerCollectionImpl();
		
		for (FootballPlayer p : players.getPlayers()) {
			if (p.getPlayingSkill() > median) {
				collection.add(p);
			}
		}
		
		return collection;
	}
	
	public static FootballPlayer retrieveLowSkilledPlayer(SimpleFootballPlayerCollection players, PlayingPosition playingPos) {
		FootballPlayer lowSkilled = null;
		
		for (FootballPlayer p : players.getPlayers()) {
			if (p.getPlayingPosition() == playingPos) {
				if ((lowSkilled == null) || (lowSkilled.getPlayingSkill() > p.getPlayingSkill())) {
					lowSkilled = p;
				}
			}
		}
		
		return lowSkilled;
	}
	
	public static SimpleFootballPlayerCollection retrievePlayingPositionPlayers(SimpleFootballPlayerCollection players, PlayingPosition playingPos) {
		int median = calculateMedianValue(players);
		
		SimpleFootballPlayerCollection collection = new ResizableSimpleFootballPlayerCollectionImpl();
		
		for (FootballPlayer p : players.getPlayers()) {
			if (p.getPlayingPosition() == playingPos) {
				collection.add(p);
			}
		}
		
		return collection;
	}
	
	private static int calculateMedianValue(SimpleFootballPlayerCollection players) {
		int skillSum = 0;
		for (FootballPlayer p : players.getPlayers()) {
			skillSum += p.getPlayingSkill();
		}
		double avgSkill = (double)skillSum / (double)players.size();
		
		double diff = -1.0;
		int median = 0;
		for (FootballPlayer p : players.getPlayers()) {
			if ((diff < 0.0) || (diff > Math.abs(avgSkill - p.getPlayingSkill()))) {
				diff = Math.abs(avgSkill - p.getPlayingSkill());
				median = p.getPlayingSkill();
			}
		}
		
		return median;
	}
	
}
