package hr.fer.oop.lab2;

import hr.fer.oop.lab2.welcomepack.Constants;
import hr.fer.oop.lab2.welcomepack.PlayingPosition;

/**
 * Vježba za studente: dokumentirajte programski kod pisanjem smislenih Javadoc komentara (razred i metode).
 */
public class FootballPlayer extends Person {

    private int playingSkill = Constants.DEFAULT_PLAYING_SKILL;
    private PlayingPosition playingPosition = Constants.DEFAULT_PLAYING_POSITION;

    public FootballPlayer(){
        super();
    }

    public FootballPlayer(String name, String country, int emotion, int playingSkill, PlayingPosition playingPosition) {
        super(name, country, emotion);

        setPlayingSkill(playingSkill);
        setPlayingPosition(playingPosition);
    }

    public void setPlayingSkill(int playingSkill) {
        if (playingSkill >= Constants.MIN_PLAYING_SKILL && playingSkill <= Constants.MAX_PLAYING_SKILL)
            this.playingSkill = playingSkill;
        else System.err.println("Igraceva vjestina je izvan raspona! :(");
    }

    public void setPlayingPosition(PlayingPosition playingPosition) {
        if(playingPosition!=null)
            this.playingPosition = playingPosition;
        else System.err.println("Igraceva pozicija je null! :(");
    }

    public int getPlayingSkill() {
        return playingSkill;
    }

    public PlayingPosition getPlayingPosition() {
        return playingPosition;
    }
}
