package hr.fer.oop.lab4.third;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Mask stream decorator wraps around an output stream object and encrypts/decrypts the streamed data by xor-ing every byte with a key.
 * @author Anthony
 * @version 1.0.0.0
 */
public class MaskStream extends OutputStream {

	private OutputStream outputStream;
	private byte key;
	
	/**
	 * Creates a new mask stream object.
	 * @param outputStream Output stream.
	 * @param key Key of encryption/decryption.
	 */
	public MaskStream(OutputStream outputStream, byte key) {
		setOutputStream(outputStream);
		setKey(key);
	}
	
	/**
	 * Gets the output stream.
	 * @return Output stream.
	 */
	public OutputStream getOutputStream() {
		return outputStream;
	}
	
	/**
	 * Sets the output stream.
	 * @param outputStream Output stream.
	 */
	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}
	
	/**
	 * Gets the key of encryption/decryption.
	 * @return Key of encryption/decryption.
	 */
	public byte getKey() {
		return key;
	}
	
	/**
	 * Sets the key of encryption/decryption.
	 * @param key Key of encryption/decryption.
	 */
	public void setKey(byte key) {
		this.key = key;
	}
	
	@Override
	public void write(int b) throws IOException {
		outputStream.write(b ^ key);
	}

}
