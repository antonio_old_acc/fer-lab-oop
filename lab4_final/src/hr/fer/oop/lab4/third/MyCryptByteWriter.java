package hr.fer.oop.lab4.third;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

import hr.fer.oop.lab4.first.MyByteWriter;

/**
 * Cryptographic byte writer reads an entire input stream and writes read data to a file, encrypted/decrypted with a key.
 * @author Anthony
 * @version 1.0.0.0
 */
public class MyCryptByteWriter extends MyByteWriter {
	
	private byte key;
	
	/**
	 * Creates a new cryptographic byte writer object.
	 * @param inputStream Input stream from which the data will be read.
	 * @param path Path of file to which the data will be written.
	 * @param key Key of encryption/decryption.
	 */
	public MyCryptByteWriter(InputStream inputStream, Path path, byte key) {
		this(inputStream, path, key, DEFAULT_APPEND);
	}
	
	/**
	 * Creates a new cryptographic byte writer object.
	 * @param inputStream Input stream from which the data will be read.
	 * @param path Path of file to which the data will be written.
	 * @param key Key of encryption/decryption.
	 * @param append Whether the data will be appended to the end of file (if the file already exists) or not.
	 */
	public MyCryptByteWriter(InputStream inputStream, Path path, byte key, boolean append) {
		super(inputStream, path, append);
		setKey(key);
	}
	
	/**
	 * Gets the key of encryption/decryption.
	 * @return Key of encryption/decryption.
	 */
	public byte getKey() {
		return key;
	}
	
	/**
	 * Sets the key of encryption/decryption.
	 * @param key Key of encryption/decryption.
	 */
	public void setKey(byte key) {
		this.key = key;
	}

	@Override
	protected OutputStream wrapOutputStream(OutputStream outputStream) {
		return new BufferedOutputStream(new MaskStream(outputStream, key), BUFFER_LENGTH);
	}
	
}
