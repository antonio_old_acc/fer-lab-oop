package hr.fer.oop.lab4.third;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * The main (entry) class of task 3.
 * @author Anthony
 * @version 1.0.0.0
 */
public abstract class Program {

	private static final byte KEY = 0x69;
	
	private Program() { }
	
	/**
	 * The main (entry) method of task 3.
	 * @param args An array of arguments passed via the command-line or the terminal.
	 */
	public static void main(String[] args) {
		
		final Path inputPath = Paths.get("input.txt");
		final Path encryptedPath = Paths.get("encrypted.txt");
		final Path decryptedPath = Paths.get("decrypted.txt");
		
		try {
			
			cryptData(inputPath, encryptedPath);
			cryptData(encryptedPath, decryptedPath);
			
		} catch (Exception ex) {
			System.err.println("Program failed: " + ex.getMessage());
			System.exit(-1);
		}
		
		System.out.println("Program complete!");
		
	}
	
	private static void cryptData(Path input, Path output) throws IOException {
		
		try (InputStream inputStream = new FileInputStream(input.toFile())) {
			(new MyCryptByteWriter(inputStream, output, KEY)).run();
		}
		
	}
	
}
