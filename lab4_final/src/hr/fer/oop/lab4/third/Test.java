package hr.fer.oop.lab4.third;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * The main (entry) test class of task 3.
 * @author Anthony
 * @version 1.0.0.0
 */
public abstract class Test {

	private Test() { }
	
	/**
	 * The main (entry) test method of task 3.
	 * @param args An array of arguments passed via the command-line or the terminal.
	 * @throws IOException If reading any of the files fails.
	 */
	public static void main(String[] args) throws IOException {
		
		final Path inputPath = Paths.get("input.txt");
		final Path encryptedPath = Paths.get("encrypted.txt");
		final Path decryptedPath = Paths.get("decrypted.txt");
		
		assertFileExists(inputPath);
		assertFileExists(encryptedPath);
		assertFileExists(decryptedPath);
		
		assertFileLengthEqual(inputPath, encryptedPath);
		assertFileLengthEqual(inputPath, decryptedPath);
		
		byte[] input = Files.readAllBytes(inputPath);
		byte[] output = Files.readAllBytes(decryptedPath);
		
		assertBufferEqual(input, output);
		
		System.out.println("Test OK.");
		
	}
	
	private static void assertFileExists(Path file) {
		if (!file.toFile().exists()) {
			System.err.println("'" + file.toString() + "' doesn't exist.");
			System.exit(-1);
		}
	}
	
	private static void assertFileLengthEqual(Path file1, Path file2) {
		if (file1.toFile().length() != file2.toFile().length()) {
			System.err.println("'" + file1.toString() + "' and '" + file2.toString() + "' length is not the same.");
			System.exit(-1);
		}
	}
	
	private static void assertBufferEqual(byte[] buffer1, byte[] buffer2) {
		if (buffer1.length != buffer2.length) {
			System.err.println("Buffer lengths are different.");
			System.exit(-1);
		}
		for (int i = 0; i < buffer1.length; i++) {
			if (buffer1[i] != buffer2[i]) {
				System.err.println("Buffers are not equal.");
				System.exit(-1);
			}
		}
	}
	
}
