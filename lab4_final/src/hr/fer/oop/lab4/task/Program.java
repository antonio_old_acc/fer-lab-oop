package hr.fer.oop.lab4.task;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class Program {

	public static final String OUTPUT_FILE = "troskovnik.txt";
	
	public static void main(String[] args) {
		
		try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(OUTPUT_FILE, false)))) {
			(new ReceiptWalker(bw)).run();
		} catch (IOException ex) {
			System.err.println("Program failed: " + ex.getMessage());
			System.exit(-1);
		}
		
		System.out.println("Program completed!");
		
	}

}
