package hr.fer.oop.lab4.task;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.TreeMap;

public class ReceiptWalker extends SimpleFileVisitor<Path> {
	
	private static final Path DEFAULT_RECEIPT_PATH = Paths.get("racuni");
	
	private BufferedWriter bufferedWriter;
	private Path receiptPath;
	private double expense = 0.0;
	private TreeMap<Integer, String> map = new TreeMap<>();
	
	public ReceiptWalker(BufferedWriter bufferedWriter) {
		this(bufferedWriter, DEFAULT_RECEIPT_PATH);
	}
	
	public ReceiptWalker(BufferedWriter bufferedWriter, Path receiptPath) {
		this.bufferedWriter = bufferedWriter;
		this.receiptPath = receiptPath;
	}
	
	public void run() throws IOException {
		Files.walkFileTree(receiptPath, this);
		for (String value : map.values()) {
			System.out.print(value);
			bufferedWriter.write(value);
		}
	}
	
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		
		if (file.toString().endsWith(".txt")) { // tolower mozda, ali ipak je ovo linux
			
			try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.toFile())))) {
				
				for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
				
					String trimmedLine = line.trim();
					
					if (trimmedLine.startsWith("UKUPNO")) {
						
						try {
							Double receiptExpense = Double.parseDouble(line.substring(105, 115));
							expense += receiptExpense;
						} catch (Exception ex) {
							// Do nothing
						}
						
					}
					
				}
				
			}
			
		}
		
		return FileVisitResult.CONTINUE;
		
	}
	
	@Override
	public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		
		String[] parts = dir.toString().split(File.separator);
		
		try {
			int year = Integer.parseInt(parts[parts.length - 2]);
			int month = Integer.parseInt(parts[parts.length - 1]);
			if ((month < 0) || (month > 12)) {
				throw new Exception();
			}
			map.put(year * 100 + month, Integer.toString(year) + " " + Integer.toString(month) + " = " + Double.toString(expense) + System.lineSeparator());
		} catch (Exception ex) {
			// Do nothing
		}
		
		expense = 0.0;
		
		return FileVisitResult.CONTINUE;
	}
	
}
