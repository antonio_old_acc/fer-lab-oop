package hr.fer.oop.lab4.first;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * Byte reader searches for all text files in a directory, reads them and writes all read data to a single output stream.
 * @author Anthony
 * @version 1.0.0.0
 */
public class MyByteReader extends SimpleFileVisitor<Path> {

	protected static final Path DEFAULT_SOURCE_DIRECTORY = Paths.get("racuni");
	protected static final int BUFFER_LENGTH = 1024;
	protected static final boolean PRINT_STATUS = true;
	
	private OutputStream outputStream;
	private BufferedOutputStream bufferedOutputStream;
	private Path sourceDirectory;
	
	/**
	 * Creates a new byte reader object.
	 * @param outputStream Output stream to where all read data will be written.
	 * @throws FileNotFoundException If the path to the directory containing text files doesn't exist or is not a directory.
	 */
	public MyByteReader(OutputStream outputStream) throws FileNotFoundException {
		this(outputStream, DEFAULT_SOURCE_DIRECTORY);
	}
	
	/**
	 * Creates a new byte reader object.
	 * @param outputStream Output stream to where all read data will be written.
	 * @param sourceDirectory Path to the directory which contains the text files that will be read.
	 * @throws FileNotFoundException If the path to the directory containing text files doesn't exist or is not a directory.
	 */
	public MyByteReader(OutputStream outputStream, Path sourceDirectory) throws FileNotFoundException {
		setOutputStream(outputStream);
		setSourceDirectory(sourceDirectory);
	}
	
	/**
	 * Search for all text files in the source directory, read them and write all read data to the output stream.
	 * @throws IOException If reading from any of the files or writing to the output stream fails.
	 */
	public void run() throws IOException {
		Files.walkFileTree(sourceDirectory, this);
	}
	
	/**
	 * Gets the output stream to where all read data will be written.
	 * @return Output stream to where all read data will be written.
	 */
	public OutputStream getOutputStream() {
		return outputStream;
	}
	
	/**
	 * Sets the output stream to where all read data will be written.
	 * @param outputStream Output stream to where all read data will be written.
	 */
	public void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
		bufferedOutputStream = new BufferedOutputStream(this.outputStream);
	}
	
	/**
	 * Gets the path to the directory which contains the text files that will be read.
	 * @return Path to the directory which contains the text files that will be read.
	 */
	public Path getSourceDirectory() {
		return sourceDirectory;
	}
	
	/**
	 * Sets the path to the directory which contains the text files that will be read.
	 * @param sourceDirectory Path to the directory which contains the text files that will be read.
	 * @throws FileNotFoundException If the path to the directory containing text files doesn't exist or is not a directory.
	 */
	public void setSourceDirectory(Path sourceDirectory) throws FileNotFoundException {
		if ((!sourceDirectory.toFile().exists()) || (!sourceDirectory.toFile().isDirectory())) {
			throw new FileNotFoundException(sourceDirectory.toString() + " (No such directory)");
		}
		this.sourceDirectory = sourceDirectory;
	}
	
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		
		if (file.toString().endsWith(".txt")) {
			
			if (PRINT_STATUS) {
				System.out.println("Found file: " + file.toString());
			}
			
			byte[] buffer = new byte[BUFFER_LENGTH];
			
			try (BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file.toFile()), BUFFER_LENGTH)) {
				for (int length = bufferedInputStream.read(buffer); length > 0; length = bufferedInputStream.read(buffer)) {
					bufferedOutputStream.write(buffer, 0, length);
				}
			}
			
		} else {
			
			if (PRINT_STATUS) {
				System.out.println("Skipping file: " + file.toString());
			}
			
		}
		
		return FileVisitResult.CONTINUE;
		
	}
	
}
