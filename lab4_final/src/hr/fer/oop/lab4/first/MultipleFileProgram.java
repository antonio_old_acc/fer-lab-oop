package hr.fer.oop.lab4.first;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * The main (entry) class for the second part of task 1.
 * @author Anthony
 * @version 1.0.0.0
 */
public abstract class MultipleFileProgram {

	private MultipleFileProgram() { }
	
	/**
	 * The main (entry) method for the second part of task 1.
	 * @param args An array of arguments passed via the command-line or the terminal.
	 */
	public static void main(String[] args) {
		
		final Path outputPath = Paths.get("multiout.txt");
		
//		if (!outputPath.toFile().exists()) {
//			try {
//				outputPath.toFile().createNewFile();
//			} catch (Exception ex) {
//				System.err.println("Program failed: " + ex.getMessage());
//				System.exit(-1);
//			}
//		}
		
		try (OutputStream outputStream = new FileOutputStream(outputPath.toFile(), false)) {
			(new MyByteReader(outputStream)).run();
		} catch (Exception ex) {
			System.err.println("Program failed: " + ex.getMessage());
			System.exit(-1);
		}
		
		System.out.println("Program complete!");
		
	}
	
}
