package hr.fer.oop.lab4.first;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

/**
 * Byte writer reads an entire input stream and writes read data to a file.
 * @author Anthony
 * @version 1.0.0.0
 */
public class MyByteWriter {

	protected static final boolean DEFAULT_APPEND = false;
	protected static final int BUFFER_LENGTH = 1024;
	
	private InputStream inputStream;
	private Path path;
	private boolean append;
	
	/**
	 * Creates a new byte writer object.
	 * @param inputStream Input stream from which the data will be read.
	 * @param path Path of file to which the data will be written.
	 */
	public MyByteWriter(InputStream inputStream, Path path) {
		this(inputStream, path, DEFAULT_APPEND);
	}
	
	/**
	 * Creates a new byte writer object.
	 * @param inputStream Input stream from which the data will be read.
	 * @param path Path of file to which the data will be written.
	 * @param append Whether the data will be appended to the end of file (if the file already exists) or not.
	 */
	public MyByteWriter(InputStream inputStream, Path path, boolean append) {
		setInputStream(inputStream);
		setPath(path);
		setAppend(append);
	}
	
	/**
	 * Read the entire input stream and write read data to the file.
	 * @throws IOException If reading from the input stream or writing to the output file fails.
	 */
	public void run() throws IOException {
		
		BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, BUFFER_LENGTH);
		byte[] buffer = new byte[BUFFER_LENGTH];
		
		try (OutputStream outputStream = wrapOutputStream(new FileOutputStream(path.toFile(), append))) {
			
			for (int length = bufferedInputStream.read(buffer); length > 0; length = bufferedInputStream.read(buffer)) {
				outputStream.write(buffer, 0, length);
			}
			
		}
		
	}
	
	/**
	 * Gets the input stream from which the data will be read.
	 * @return Input stream from which the data will be read.
	 */
	public InputStream getInputStream() {
		return inputStream;
	}
	
	/**
	 * Sets the input stream from which the data will be read.
	 * @param inputStream Input stream from which the data will be read.
	 */
	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	
	/**
	 * Gets the path of file to which the data will be written.
	 * @return Path of file to which the data will be written.
	 */
	public Path getPath() {
		return path;
	}

	/**
	 * Sets the path of file to which the data will be written.
	 * @param path Path of file to which the data will be written.
	 */
	public void setPath(Path path) {
		this.path = path;
	}
	
	/**
	 * Gets whether the data will be appended to the end of file (if the file already exists) or not.
	 * @return True if the data will be appended to the end of file (if the file already exists) and false otherwise.
	 */
	public boolean getAppend() {
		return append;
	}
	
	/**
	 * Sets whether the data will be appended to the end of file (if the file already exists) or not.
	 * @param append Whether the data will be appended to the end of file (if the file already exists) or not.
	 */
	public void setAppend(boolean append) {
		this.append = append;
	}
	
	protected OutputStream wrapOutputStream(OutputStream outputStream) {
		return new BufferedOutputStream(outputStream, BUFFER_LENGTH);
	}
	
}
