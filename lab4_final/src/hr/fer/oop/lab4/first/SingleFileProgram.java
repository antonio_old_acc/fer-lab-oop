package hr.fer.oop.lab4.first;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * The main (entry) class for the first part of task 1.
 * @author Anthony
 * @version 1.0.0.0
 */
public abstract class SingleFileProgram {

	private SingleFileProgram() { }
	
	/**
	 * The main (entry) method for the first part of task 1.
	 * @param args An array of arguments passed via the command-line or the terminal.
	 */
	public static void main(String[] args) {
		
		final Path inputPath = Paths.get("racuni/2015/1/Racun_0.txt");
		final Path outputPath = Paths.get("singleout.txt");
		
		try (InputStream inputStream = new FileInputStream(inputPath.toFile())) {
			(new MyByteWriter(inputStream, outputPath)).run();
		} catch (Exception ex) {
			System.err.println("Program failed: " + ex.getMessage());
			System.exit(-1);
		}
		
		System.out.println("Program complete!");
		
	}
	
}
