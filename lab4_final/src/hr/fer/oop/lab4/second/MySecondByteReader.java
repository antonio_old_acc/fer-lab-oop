package hr.fer.oop.lab4.second;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Set;
import java.util.TreeSet;

import hr.fer.oop.lab4.first.MyByteReader;

/**
 * Byte reader searches for all text files in a directory, reads them and writes all found article alongside their prices to a file.
 * @author Anthony
 * @version 1.0.0.0
 */
public class MySecondByteReader extends MyByteReader {

	private static class NullOutputStream extends OutputStream {

		@Override
		public void write(int b) throws IOException { }
		
	}
	
	private Set<Article> articleSet;
	
	/**
	 * Creates a new byte reader object.
	 * @throws FileNotFoundException If the path to the directory containing text files doesn't exist or is not a directory.
	 */
	public MySecondByteReader() throws FileNotFoundException {
		this(DEFAULT_SOURCE_DIRECTORY);
	}

	/**
	 * Creates a new byte reader object.
	 * @param sourceDirectory Path to the directory which contains the text files that will be read.
	 * @throws FileNotFoundException If the path to the directory containing text files doesn't exist or is not a directory.
	 */
	public MySecondByteReader(Path sourceDirectory) throws FileNotFoundException {
		super(new NullOutputStream(), sourceDirectory);
		articleSet = new TreeSet<>();
	}
	
	/**
	 * Gets the set of articles.
	 * @return Set of articles.
	 */
	public Set<Article> getArticleSet() {
		return articleSet;
	}
	
	@Override
	public void run() throws IOException {
		articleSet.clear();
		super.run();
	}
	
	@Override
	public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
		
		if (file.toString().endsWith(".txt")) {
			
			if (PRINT_STATUS) {
				System.out.println("Found file: " + file.toString());
			}
			
			try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.toFile())), BUFFER_LENGTH)) {
				
				for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
				
					String trimmedLine = line.trim();
					
					if ((!trimmedLine.startsWith("Račun")) && (!trimmedLine.startsWith("Proizvod")) && (!trimmedLine.startsWith("-")) && (!trimmedLine.startsWith("UKUPNO")) && (trimmedLine.length() >= 90)) {
						
						String nameSegment = line.substring(0, 80);
						String priceSegment = line.substring(80, 90);
						
						try {
							
							Article article = new Article(nameSegment.trim(), Double.parseDouble(priceSegment));
							boolean added = articleSet.add(article);
							
							if ((PRINT_STATUS) && (added)) {
								System.out.println("Found new article: " + article.toString());
							}
							
						} catch (Exception ex) { }
						
					}
					
				}
				
			}
			
		} else {
			
			if (PRINT_STATUS) {
				System.out.println("Skipping file: " + file.toString());
			}
			
		}
		
		return FileVisitResult.CONTINUE;
	}
	
}
