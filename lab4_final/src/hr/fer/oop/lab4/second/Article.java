package hr.fer.oop.lab4.second;

/**
 * Article represents an item in the store.
 * @author Anthony
 * @version 1.0.0.0
 */
public class Article implements Comparable<Article> {
	
	private String name;
	private double price;
	
	/**
	 * Creates a new article object.
	 * @param name Name of the article.
	 * @param price Price of the article.
	 */
	public Article(String name, double price) {
		setName(name);
		setPrice(price);
	}
	
	/**
	 * Gets the name of the article.
	 * @return Name of the article.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name of the article.
	 * @param name Name of the article.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets the price of the article.
	 * @return Price of the article.
	 */
	public double getPrice() {
		return price;
	}
	
	/**
	 * Sets the price of the article.
	 * @param price Price of the article.
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return String.format("%s (%.2f)", name, price);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Article) {
			return name.equals(((Article)obj).name);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public int compareTo(Article o) {
		return name.compareTo(o.name);
	}
	
}
