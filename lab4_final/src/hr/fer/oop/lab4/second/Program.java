package hr.fer.oop.lab4.second;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import org.omg.CORBA.Environment;

/**
 * The main (entry) class of task 2.
 * @author Anthony
 * @version 1.0.0.0
 */
public abstract class Program {

	private Program() { }
	
	/**
	 * The main (entry) method of task 2.
	 * @param args An array of arguments passed via the command-line or the terminal.
	 */
	public static void main(String[] args) {
		
		try (
			BufferedWriter bufferedWriter1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("cjenik.88592.txt", false), Charset.forName("ISO-8859-2")));
			BufferedWriter bufferedWriter2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("cjenik.utf8.txt", false), StandardCharsets.UTF_8));
		) {
			
			MySecondByteReader mySecondByteReader = new MySecondByteReader();
			mySecondByteReader.run();
			
			Set<Article> articleSet = mySecondByteReader.getArticleSet();
			
			for (Article article : articleSet) {
				bufferedWriter1.write(article.toString() + System.lineSeparator());
				bufferedWriter2.write(article.toString() + System.lineSeparator());
			}
			
		} catch (Exception ex) {
			System.err.println("Program failed: " + ex.getMessage());
			System.exit(-1);
		}
		
		System.out.println("Program complete!");
		
	}
	
}
