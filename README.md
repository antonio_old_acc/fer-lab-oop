# FER OOP Lab Collection
Created by Antonio Junakovic

# Manual compilation and JAR packaging commands
    cd .
    javac -cp ./lib/library.jar -sourcepath ./src -d ./bin src/com/name/Class1.java
    cd ./bin
    jar -cfve ./../dist/output.jar com.name.Class1 com
    cd ./../dist
    java -cp ./output.jar:./../lib/library.jar com.name.Class1

# Javadoc generation
    javadoc -sourcepath ./src -d ./doc com.name.Package1 com.name.Package2 ...

