package hr.fer.oop.lab5.task2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

/**
 * Game of life board window.
 * @author Anthony
 */
public class BoardFrame extends JFrame {

	private static final long serialVersionUID = 3914404008285747392L;
	private static final int WIDTH = 30;
	private static final int HEIGHT = 30;
	
	private static Random randomizer = new Random();
	
	private Board board;
	private JPanel pnTop;
	private JButton bnStart;
	private JButton bnStop;
	private JButton bnStep;
	private JButton bnRandomize;
	private JButton bnClear;
	private JButton bnGallery;
	private JPanel pnGrid;
	private JToggleButton[][] tbCells;
	
	private GalleryFrame gfMain;
	
	/**
	 * Creates a new game of life board window.
	 */
	public BoardFrame() {
		this(true);
	}
	
	/**
	 * Creates a new game of life board window.
	 * @param centerWindow Determines whether the window will be centered on screen or not.
	 */
	public BoardFrame(boolean centerWindow) {
		board = new Board(WIDTH, HEIGHT);
		board.addListener(this::board_Changed);
		
		pnTop = new JPanel(new FlowLayout());
		this.add(pnTop, BorderLayout.NORTH);
		
		bnStart = new JButton("Pokreni");
		bnStart.addActionListener(this::bnStart_Action);
		pnTop.add(bnStart);
		
		bnStop = new JButton("Zaustavi");
		bnStop.addActionListener(this::bnStop_Action);
		bnStop.setEnabled(false);
		pnTop.add(bnStop);
		
		bnStep = new JButton("Jedna Iteracija");
		bnStep.addActionListener(this::bnStep_Action);
		pnTop.add(bnStep);
		
		bnRandomize = new JButton("Nasumično Polje");
		bnRandomize.addActionListener(this::bnRandomize_Action);
		pnTop.add(bnRandomize);
		
		bnClear = new JButton("Počisti");
		bnClear.addActionListener(this::bnClear_Action);
		pnTop.add(bnClear);
		
		bnGallery = new JButton("Galerija uzoraka");
		bnGallery.addActionListener(this::bnGallery_Action);
		pnTop.add(bnGallery);
		
		pnGrid = new JPanel(new GridLayout(WIDTH, 0));
		this.add(pnGrid, BorderLayout.CENTER);
		
		gfMain = new GalleryFrame(this, centerWindow, WIDTH, HEIGHT);
		
		tbCells = new JToggleButton[WIDTH][HEIGHT];
		for (int j = 0; j < HEIGHT; j++) {
			for (int i = 0 ; i < WIDTH; i++) {
				int x = i;
				int y = j;
				tbCells[x][y] = new JToggleButton();
				tbCells[x][y].setMinimumSize(new Dimension(20, 20));
				tbCells[x][y].setPreferredSize(new Dimension(20, 20));
				tbCells[x][y].addActionListener((e) -> tbCells_Action(e, x, y));
				tbCells[x][y].setBackground(Color.BLACK);
				pnGrid.add(tbCells[x][y]);
			}
		}
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setTitle("Game of Life");
		this.setMinimumSize(new Dimension((int)this.getPreferredSize().getWidth() + 32, (int)this.getPreferredSize().getHeight() + 32));
		if (centerWindow) {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension windowSize = this.getSize();
			this.setLocation((int)(screenSize.getWidth() - windowSize.getWidth()) / 2, (int)(screenSize.getHeight() - windowSize.getHeight()) / 2);
		}
		this.setVisible(true);
	}
	
	private boolean executing = false;
	
	private void enableControls(boolean enabled) {
		bnStart.setEnabled(enabled);
		bnStop.setEnabled(!enabled);
		bnStep.setEnabled(enabled);
		bnRandomize.setEnabled(enabled);
		bnClear.setEnabled(enabled);
//		for (int i = 0; i < WIDTH; i++) {
//			for (int j = 0; j < HEIGHT; j++) {
//				tbCells[i][j].setEnabled(enabled);
//			}
//		}
	}
	
	private void bnStart_Action(ActionEvent e) {
		enableControls(false);
		executing = true;
		(new Thread(() -> {
			int step = 0;
			while (executing) {
				board.playOneIteration();
				try {
					Thread.sleep(100);
				} catch (Exception ex) {
				}
			}
			try {
				SwingUtilities.invokeAndWait(() -> {
					enableControls(true);
				});
			} catch (Exception ex) {
			}
		})).start();
	}
	
	private void bnStop_Action(ActionEvent e) {
		executing = false;
	}
	
	private void bnStep_Action(ActionEvent e) {
		board.playOneIteration();
	}
	
	private void bnRandomize_Action(ActionEvent e) {
		for (int i = 0; i < WIDTH; i++) {
			for (int j = 0; j < HEIGHT; j++) {
				boolean alive = randomizer.nextBoolean();
				tbCells[i][j].setSelected(alive);
				board.setCell(i, j, alive);
			}
		}
	}
	
	private void bnClear_Action(ActionEvent e) {
		for (int i = 0; i < WIDTH; i++) {
			for (int j = 0; j < HEIGHT; j++) {
				tbCells[i][j].setSelected(false);
				board.setCell(i, j, false);
			}
		}
	}
	
	private void bnGallery_Action(ActionEvent e) {
		gfMain.setVisible(true);
		if (gfMain.isOK()) {
			boolean[][] schematic = GalleryFrame.Entity.getSchematic(gfMain.getEntity());
			int x = gfMain.getX();
			int y = gfMain.getY();
			for (int i = 0; i < schematic[0].length; i++) {
				for (int j = 0; j < schematic.length; j++) {
					boolean value = schematic[j][i];
					tbCells[i + x][j + y].setSelected(value);
					board.setCell(i + x, j + y, value);
				}
			}
		}
	}
	
	private void tbCells_Action(ActionEvent e, int x, int y) {
		if (!executing) {
			board.setCell(x, y, tbCells[x][y].isSelected());
		}
	}
	
	private void board_Changed(Board board) {
		for (int i = 0; i < WIDTH; i++) {
			for (int j = 0; j < HEIGHT; j++) {
				tbCells[i][j].setSelected(board.isCellAlive(i, j));
			}
		}
	}
	
}
