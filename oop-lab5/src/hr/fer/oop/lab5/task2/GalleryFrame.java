package hr.fer.oop.lab5.task2;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import hr.fer.oop.spring.SpringUtilities;

public class GalleryFrame extends JDialog {
	
	public static enum Entity {
		Beacon,
		Glider,
		Acorn;
		
		public static Entity getFromString(String name) {
			switch (name.toLowerCase()) {
			case "beacon":
				return Beacon;
			case "glider":
				return Glider;
			case "acorn":
				return Acorn;
			}
			return null;
		}
		
		public static boolean[][] getSchematic(Entity entity) {
			switch (entity) {
			case Beacon:
				return BeaconArray;
			case Glider:
				return GliderArray;
			case Acorn:
				return AcornArray;
			}
			return null;
		}
		
		private static final boolean[][] BeaconArray = new boolean[][] {
			{true, true, false, false},
			{true, false, false, false},
			{false, false, false, true},
			{false, false, true, true}
		};
		
		private static final boolean[][] GliderArray = new boolean[][] {
			{false, true, false},
			{false, false, true},
			{true, true, true}
		};
		
		private static final boolean[][] AcornArray = new boolean[][] {
			{false, true, false, false, false, false, false},
			{false, false, false, true, false, false, false},
			{true, true, false, false, true, true, true}
		};
	}
	
	private static final long serialVersionUID = -1575317699326043029L;
	
	private int limitX, limitY;
	
	private boolean ok = false;
	private int x = 0, y = 0;
	private Entity entity = null;
	
	private JLabel lbCoordX;
	private JTextField tfCoordX;
	private JLabel lbCoordY;
	private JTextField tfCoordY;
	private JLabel lblEntity;
	private JComboBox<String> cbEntity;
	private JButton bnOK;
	private JButton bnCancel;
	
	public GalleryFrame(JFrame owner, boolean centerWindow, int limitX, int limitY) {
		super(owner, true);
		
		this.limitX = limitX;
		this.limitY = limitY;
		
		lbCoordX = new JLabel("X koordinata");
		this.add(lbCoordX);
		
		tfCoordX = new JTextField(5);
		this.add(tfCoordX);
		
		lbCoordY = new JLabel("Y koordinata");
		this.add(lbCoordY);
		
		tfCoordY = new JTextField(5);
		this.add(tfCoordY);
		
		lblEntity = new JLabel("Oblik");
		this.add(lblEntity);
		
		cbEntity = new JComboBox<String>(new String[] {"Beacon", "Glider", "Acorn"});
		this.add(cbEntity);
		
		bnOK = new JButton("OK");
		bnOK.addActionListener(this::bnOK_Action);
		this.add(bnOK);
		
		bnCancel = new JButton("Cancel");
		bnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				ok = false;
				setVisible(false);
			}
		});
		this.add(bnCancel);
		
		this.getContentPane().setLayout(new SpringLayout());
		SpringUtilities.makeCompactGrid(this.getContentPane(), 4, 2, 4, 4, 4, 4);
		
		this.setTitle("Galerija Uzoraka");
		this.setMinimumSize(new Dimension((int)this.getPreferredSize().getWidth() + 32, (int)this.getPreferredSize().getHeight() + 32));
		this.setMaximumSize(this.getMinimumSize());
		this.setResizable(false);
		if (centerWindow) {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension windowSize = this.getSize();
			this.setLocation((int)(screenSize.getWidth() - windowSize.getWidth()) / 2, (int)(screenSize.getHeight() - windowSize.getHeight()) / 2);
		}
	}
	
	@Override
	public void setVisible(boolean b) {
		if (b) {
			ok = false;
		}
		super.setVisible(b);
	}
	
	public boolean isOK() {
		return ok;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public Entity getEntity() {
		return entity;
	}
	
	private void bnOK_Action(ActionEvent e) {
		try {
			x = Integer.valueOf(tfCoordX.getText());
			if ((x < 0) || (x >= limitX)) {
				throw new Exception();
			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(this, "Coordinate X is invalid.", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		try {
			y = Integer.valueOf(tfCoordY.getText());
			if ((y < 0) || (y >= limitY)) {
				throw new Exception();
			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(this, "Coordinate Y is invalid.", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		entity = Entity.getFromString((String)cbEntity.getSelectedItem());
		boolean[][] schematic = Entity.getSchematic(entity);
		if (((x + schematic[0].length) > limitX) || ((y + schematic.length) > limitY)) {
			JOptionPane.showMessageDialog(this, "Entity cannot fit to that location.", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		ok = true;
		setVisible(false);
	}
	
	public void bnCancel_Action(ActionEvent e) {
		
	}
	
}
