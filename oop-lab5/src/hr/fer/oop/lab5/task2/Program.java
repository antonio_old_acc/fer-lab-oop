package hr.fer.oop.lab5.task2;

import javax.swing.SwingUtilities;

/**
 * The entry class of the second task.
 * @author Anthony
 */
public class Program {

	/**
	 * The entry method of the second task.
	 * @param args Arguments passed via the console.
	 */
	public static void main(String[] args) {
		try {
			SwingUtilities.invokeAndWait(BoardFrame::new);
		} catch (Exception ex) {
			System.err.println("EXECUTION FAILED: " + ex.getMessage());
			System.exit(-1);
		}
	}

}
