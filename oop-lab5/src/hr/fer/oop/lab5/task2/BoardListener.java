package hr.fer.oop.lab5.task2;

/**
 * Board listener class.
 * @author Anthony
 */
public interface BoardListener {
	
	/**
	 * This method will be called when the board changes.
	 * @param board Board which changed.
	 */
	void boardChanged(Board board);
	
}
