package hr.fer.oop.lab5.task2;

import java.util.LinkedList;
import java.util.List;

import javax.swing.SwingUtilities;

/**
 * Represents a board for the game of life.
 * @author Anthony
 */
public class Board {

	private List<BoardListener> boardListeners = new LinkedList<>();
	private boolean[][] field;
	private boolean[][] spareField;
	private int width;
	private int height;
	
	/**
	 * Creates a new board for the game of live.
	 * @param width Board width.
	 * @param height Board height.
	 */
	public Board(int width, int height) {
		this.width = width;
		this.height = height;
		field = new boolean[width][height];
		spareField = new boolean[width][height];
	}
	
	/**
	 * Gets the board width.
	 * @return Board width.
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * Gets the board height.
	 * @return Board height.
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Returns whether cell is alive or not.
	 * @param x X coordinate of the cell.
	 * @param y Y coordinate of the cell.
	 * @return Value true if cell is alive, false if cell is dead or the coordinates are out of bounds.
	 */
	public boolean isCellAlive(int x, int y) {
		if ((x >= 0) && (x < width) && (y >= 0) && (y < height)) {
			return field[x][y];
		}
		return false;
	}
	
	/**
	 * Sets the state of the cell.
	 * @param x X coordinate of the cell.
	 * @param y Y coordinate of the cell.
	 * @param alive Value that determines the cells state.
	 */
	public void setCell(int x, int y, boolean alive) {
		if ((x >= 0) && (x < width) && (y >= 0) && (y < height)) {
			field[x][y] = alive;
		}
	}
	
	/**
	 * Counts the alive neighbors of the cell.
	 * @param x X coordinate of the cell.
	 * @param y Y coordinate of the cell.
	 * @return Alive neighbors count.
	 */
	public int countAliveNeighbors(int x, int y) {
		int count = isCellAlive(x, y) ? -1 : 0;
		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				count += isCellAlive(x + i, y + j) ? 1 : 0;
			}
		}
		return count;
	}
	
	/**
	 * Advances the game by one iteration.
	 */
	public void playOneIteration() {
		boolean[][] newField = spareField;
		spareField = field;
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int count = countAliveNeighbors(i, j);
				if (isCellAlive(i, j)) {
					if ((count < 2) || (count > 3)) {
						newField[i][j] = false;
					} else {
						newField[i][j] = true;
					}
				} else {
					if (count == 3) {
						newField[i][j] = true;
					} else {
						newField[i][j] = false;
					}
				}
			}
		}
		field = newField;
		for (BoardListener listener : boardListeners) {
			if (SwingUtilities.isEventDispatchThread()) {
				listener.boardChanged(this);
			} else {
				try {
					SwingUtilities.invokeAndWait(() -> listener.boardChanged(this));
				} catch (Exception ex) {
				}
			}
		}
	}
	
	/**
	 * Adds a board event listener.
	 * @param listener Board event listener.
	 */
	public void addListener(BoardListener listener) {
		boardListeners.add(listener);
	}
	
	/**
	 * Removes an existing board event listener.
	 * @param listener Board event listener.
	 */
	public void removeListener(BoardListener listener) {
		boardListeners.remove(listener);
	}
	
}
