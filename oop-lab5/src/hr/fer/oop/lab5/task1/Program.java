package hr.fer.oop.lab5.task1;

import javax.swing.SwingUtilities;

/**
 * The entry class of the first task.
 * @author Anthony
 */
public abstract class Program {

	/**
	 * The entry method of the first task.
	 * @param args Arguments passed via the console.
	 */
	public static void main(String[] args) {
		try {
			SwingUtilities.invokeAndWait(MainWindow::new);
		} catch (Exception ex) {
			System.err.println("EXECUTION FAILED: " + ex.getMessage());
			System.exit(-1);
		}
	}

}
