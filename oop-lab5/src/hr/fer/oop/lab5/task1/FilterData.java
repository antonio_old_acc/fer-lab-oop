package hr.fer.oop.lab5.task1;

import java.util.stream.Stream;

/**
 * Class that represents settings from filter dialog.
 * @author Anthony
 */
public class FilterData {
	
	public static enum DistanceComparator {
		LESSER, GREATER_OR_EQUAL;
		
		public static DistanceComparator parse(String distanceComparator) {
			switch (distanceComparator.toLowerCase().trim().replace(" ", "").replace("_", "")) {
			case "lesser":
			case "<":
				return LESSER;
			case "greaterorequal":
			case "gequal":
			case ">=":
			case "≥":
				return GREATER_OR_EQUAL;
			}
			return null;
		}
	}
	
	public boolean useSkipRecords;
	public int skipRecordsCount;
	public boolean useLeaveRecords;
	public int leaveRecordsCount;
	public boolean usePaymentType;
	public TaxiRecord.PaymentType paymentType;
	public boolean useLimitDistance;
	public DistanceComparator distanceComparator;
	public double distance;
	
	/**
	 * Creates a new filter data.
	 * @param useSkipRecords Determines whether skip records is used.
	 * @param useLeaveRecords Determines whether leave records is used.
	 * @param usePaymentType Determines whether payment type is used.
	 * @param useLimitDistance Determines whether limit distance is used.
	 */
	public FilterData(boolean useSkipRecords, boolean useLeaveRecords, boolean usePaymentType, boolean useLimitDistance) {
		this.useSkipRecords = useSkipRecords;
		skipRecordsCount = 0;
		this.useLeaveRecords = useLeaveRecords;
		leaveRecordsCount = 0;
		this.usePaymentType = usePaymentType;
		paymentType = TaxiRecord.PaymentType.CASH;
		this.useLimitDistance = useLimitDistance;
		distanceComparator = DistanceComparator.LESSER;
		distance = 0.0;
	}
	
	/**
	 * Applies the filter data to a stream.
	 * @param input Input stream.
	 * @return Filtered stream.
	 */
	public Stream<TaxiRecord> applyToStream(Stream<TaxiRecord> input) {
		Stream<TaxiRecord> output = input;
		if (useSkipRecords) {
			output = output.skip(skipRecordsCount);
		}
		if (useLeaveRecords) {
			output = output.limit(leaveRecordsCount);
		}
		if (usePaymentType) {
			output = output.filter(r -> r.getPaymentType() == paymentType);
		}
		if (useLimitDistance) {
			switch (distanceComparator) {
			case LESSER:
				output = output.filter(r -> r.getTripDistance() < distance);
				break;
			case GREATER_OR_EQUAL:
				output = output.filter(r -> r.getTripDistance() >= distance);
				break;
			}
		}
		return output;
	}
	
}
