package hr.fer.oop.lab5.task1;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.filechooser.FileNameExtensionFilter;

import hr.fer.oop.generictable.GenericTablePanel;

/**
 * Main workspace window class.
 * @author Anthony
 */
public class MainWindow extends JFrame {
	
	private static final long serialVersionUID = -5587754091739860622L;
	
	private FilterDialog fdMain;
	private JToolBar tbMain;
	private JButton bnExit;
	private JTextField tfFileName;
	private JButton bnBrowseFile;
	private JFileChooser fcBrowseFile;
	private JButton bnLoadFile;
	private JButton bnDefineFilter;
	private JButton bnApplyFilter;
	private GenericTablePanel<TaxiRecord> tbRecords;
	private JTextArea taLog;
	private JScrollPane spLog;
	private List<TaxiRecord> allRecords = new LinkedList<>();
	
	/**
	 * Creates a new main workspace window.
	 */
	public MainWindow() {
		this(true);
	}
	
	/**
	 * Creates a new main workspace window.
	 * @param centerScreen Determines whether the window will start in a center position or not.
	 */
	public MainWindow(boolean centerScreen) {
		fdMain = new FilterDialog(this, centerScreen);
		
		tbMain = new JToolBar(SwingConstants.HORIZONTAL);
		tbMain.setMargin(new Insets(4, 4, 4, 4));
		this.add(tbMain, BorderLayout.NORTH);
		
		bnExit = new JButton("Exit");
		bnExit.addActionListener(this::bnExit_Action);
		tbMain.add(bnExit);
		tbMain.addSeparator();
		
		tfFileName = new JTextField(32);
		tfFileName.setMaximumSize(tfFileName.getPreferredSize());
		tbMain.add(tfFileName);
		tbMain.addSeparator();
		
		bnBrowseFile = new JButton("Browse File");
		bnBrowseFile.addActionListener(this::bnBrowseFile_Action);
		tbMain.add(bnBrowseFile);
		tbMain.addSeparator();
		
		fcBrowseFile = new JFileChooser(new File("").getAbsolutePath());
		fcBrowseFile.setDialogTitle("Browse File");
		fcBrowseFile.setFileFilter(new FileNameExtensionFilter("Comma-Separated Values (CSV) File", "csv"));
		
		bnLoadFile = new JButton("Load File");
		bnLoadFile.addActionListener(this::bnLoadFile_Action);
		tbMain.add(bnLoadFile);
		tbMain.addSeparator();
		
		bnDefineFilter = new JButton("Define Filter");
		bnDefineFilter.addActionListener(this::bnDefineFilter_Action);
		tbMain.add(bnDefineFilter);
		tbMain.addSeparator();
		
		bnApplyFilter = new JButton("Apply Filter");
		bnApplyFilter.addActionListener(this::bnApplyFilter_Action);
		tbMain.add(bnApplyFilter);
		tbMain.addSeparator();
		
		tbRecords = new GenericTablePanel<>(TaxiRecord.class);
		this.add(tbRecords, BorderLayout.CENTER);
		
		taLog = new JTextArea(6, 64);
		taLog.setEditable(false);
		
		spLog = new JScrollPane(taLog);
		this.add(spLog, BorderLayout.SOUTH);
		
		this.setTitle("Taxi Record Viewer");
		this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exit();
			}
		});
		this.setMinimumSize(this.getPreferredSize());
		if (centerScreen) {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension windowSize = this.getSize();
			this.setLocation((int)(screenSize.getWidth() - windowSize.getWidth()) / 2, (int)(screenSize.getHeight() - windowSize.getHeight()) / 2);
		}
		this.setVisible(true);
	}
	
	/**
	 * Triggers the exit program procedure.
	 */
	public void exit() {
		System.exit(0);
	}
	
	/**
	 * Outputs message to the log area at the bottom of the window.
	 * @param message Message to be logged.
	 */
	public void outputLog(String message) {
		taLog.append(message + System.lineSeparator());
	}
	
	private void bnExit_Action(ActionEvent e) {
		exit();
	}
	
	private void bnBrowseFile_Action(ActionEvent e) {
		if (fcBrowseFile.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			tfFileName.setText(fcBrowseFile.getSelectedFile().toString());
		}
	}
	
	private int newRecordId = 0;
	
	private void bnLoadFile_Action(ActionEvent e) {
		Path file = Paths.get(tfFileName.getText());
		if (file.toFile().exists()) {
			tbRecords.update(new LinkedList<TaxiRecord>());
			tfFileName.setEnabled(false);
			bnBrowseFile.setEnabled(false);
			bnLoadFile.setEnabled(false);
			bnDefineFilter.setEnabled(false);
			bnApplyFilter.setEnabled(false);
			(new Thread(() -> {
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				try {
					SwingUtilities.invokeAndWait(() -> outputLog("Loading records from '" + file.toString() + "' file..."));
				} catch (Exception ex) {
				}
				try {
					newRecordId = 0;
					allRecords = Files.lines(file).map((s) -> {
						String[] parts = s.split(",");
						TaxiRecord result = null;
						try {
							result = new TaxiRecord(newRecordId, parts[0], parts[1], dateFormat.parse(parts[2]), dateFormat.parse(parts[3]), Double.parseDouble(parts[4]), Double.parseDouble(parts[5]), Double.parseDouble(parts[6]), Double.parseDouble(parts[7]), Double.parseDouble(parts[8]), Double.parseDouble(parts[9]), TaxiRecord.PaymentType.parse(parts[10]), Double.parseDouble(parts[11]), Double.parseDouble(parts[12]), Double.parseDouble(parts[13]), Double.parseDouble(parts[14]), Double.parseDouble(parts[15]), Double.parseDouble(parts[16]));
							newRecordId++;
						} catch (Exception ex) {
							try {
								SwingUtilities.invokeAndWait(() -> outputLog("RECORD PARSING FAILED: " + ex.getMessage()));
							} catch (Exception ex2) {
							}
						}
						return result;
					}).filter((r) -> r != null).collect(Collectors.toList());
					try {
						SwingUtilities.invokeAndWait(() -> {
							outputLog("Loaded " + allRecords.size() + " records.");
							tbRecords.update(allRecords);
						});
					} catch (Exception ex) {
					}
				} catch (Exception ex) {
					try {
						SwingUtilities.invokeAndWait(() -> outputLog("LOADING RECORDS FAILED: " + ex.getMessage()));
					} catch (Exception ex2) {
					}
				}
				try {
					SwingUtilities.invokeAndWait(() -> {
						tfFileName.setEnabled(true);
						bnBrowseFile.setEnabled(true);
						bnLoadFile.setEnabled(true);
						bnDefineFilter.setEnabled(true);
						bnApplyFilter.setEnabled(true);
					});
				} catch (Exception ex) {
				}
			})).start();
		} else {
			if (file.toString().isEmpty()) {
				JOptionPane.showMessageDialog(this, "File not specified.", "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(this, "The file '" + file.toString() + "' does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
			}
			outputLog("Cannot open file.");
		}
	}
	
	private void bnDefineFilter_Action(ActionEvent e) {
		fdMain.setVisible(!fdMain.isVisible());
	}
	
	private void bnApplyFilter_Action(ActionEvent e) {
		outputLog("Applying defined filter...");
		tfFileName.setEnabled(false);
		bnBrowseFile.setEnabled(false);
		bnLoadFile.setEnabled(false);
		bnDefineFilter.setEnabled(false);
		bnApplyFilter.setEnabled(false);
		(new Thread(() -> {
			Stream<TaxiRecord> stream = allRecords.stream();
			FilterData filterData = fdMain.getFilterData();
			Stream<TaxiRecord> filteredStream = filterData.applyToStream(stream);
			List<TaxiRecord> filteredList = filteredStream.collect(Collectors.toList());
			try {
				SwingUtilities.invokeAndWait(() -> {
					outputLog("Filtered " + filteredList.size() + " out of " + allRecords.size() + " records.");
					tbRecords.update(filteredList);
				});
			} catch (Exception ex) {
			}
			try {
				SwingUtilities.invokeAndWait(() -> {
					tfFileName.setEnabled(true);
					bnBrowseFile.setEnabled(true);
					bnLoadFile.setEnabled(true);
					bnDefineFilter.setEnabled(true);
					bnApplyFilter.setEnabled(true);
				});
			} catch (Exception ex) {
			}
		})).start();
	}
	
}
