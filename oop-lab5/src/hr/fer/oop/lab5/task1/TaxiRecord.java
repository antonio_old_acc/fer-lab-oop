package hr.fer.oop.lab5.task1;

import java.util.Date;

/**
 * Class which represents a single taxi record.
 * @author Anthony
 */
public class TaxiRecord {

	public static enum PaymentType {
		CARD, CASH, UNKNOWN;
		
		/**
		 * Parses the string as an enum item.
		 * @param paymentType String value of the enum item (case insensitive).
		 * @return Enum item.
		 */
		public static PaymentType parse(String paymentType) {
			switch (paymentType.toLowerCase().trim()) {
			case "card":
			case "crd":
				return CARD;
			case "cash":
			case "csh":
				return CASH;
			}
			return UNKNOWN;
		}
	};
	
	private int id;
	private String medallion;
	private String hackLicense;
	private Date pickupDatetime;
	private Date dropoffDatetime;
	private double tripTimeInSecs;
	private double tripDistance;
	private double pickupLongitude;
	private double pickupLatitude;
	private double dropoffLongitude;
	private double dropoffLatitude;
	private PaymentType paymentType;
	private double fareAmount;
	private double surcharge;
	private double mtaTax;
	private double tipAmount;
	private double tollsAmount;
	private double totalAmount;
	
	/**
	 * Creates a new taxi record.
	 * @param id An identification number of the record.
	 * @param medallion An md5sum of the identifier of the taxi - vehicle bound.
	 * @param hackLicense An md5sum of the identifier for the taxi license.
	 * @param pickupDatetime Time when the passenger(s) were picked up.
	 * @param dropoffDatetime Time when the passenger(s) were dropped off.
	 * @param tripTimeInSecs Duration of the trip.
	 * @param tripDistance Trip distance in miles.
	 * @param pickupLongitude Longitude coordinate of the pickup location.
	 * @param pickupLatitude Latitude coordinate of the pickup location.
	 * @param dropoffLongitude Longitude coordinate of the drop-off location.
	 * @param dropoffLatitude Latitude coordinate of the drop-off location.
	 * @param paymentType The payment method.
	 * @param fareAmount Fare amount in dollars.
	 * @param surcharge Surcharge in dollars.
	 * @param mtaTax Tax in dollars.
	 * @param tipAmount Tip in dollars.
	 * @param tollsAmount Bridge and tunnel tolls in dollars.
	 * @param totalAmount Total paid amount in dollars.
	 */
	public TaxiRecord(int id, String medallion, String hackLicense, Date pickupDatetime, Date dropoffDatetime, double tripTimeInSecs, double tripDistance, double pickupLongitude, double pickupLatitude, double dropoffLongitude, double dropoffLatitude, PaymentType paymentType, double fareAmount, double surcharge, double mtaTax, double tipAmount, double tollsAmount, double totalAmount) {
		this.id = id;
		this.medallion = medallion;
		this.hackLicense = hackLicense;
		this.pickupDatetime = pickupDatetime;
		this.dropoffDatetime = dropoffDatetime;
		this.tripTimeInSecs = tripTimeInSecs;
		this.tripDistance = tripDistance;
		this.pickupLongitude = pickupLongitude;
		this.pickupLatitude = pickupLatitude;
		this.dropoffLongitude = dropoffLongitude;
		this.dropoffLatitude = dropoffLatitude;
		this.paymentType = paymentType;
		this.fareAmount = fareAmount;
		this.surcharge = surcharge;
		this.mtaTax = mtaTax;
		this.tipAmount = tipAmount;
		this.tollsAmount = tollsAmount;
		this.totalAmount = totalAmount;
	}

	/**
	 * Gets the identification number of the record.
	 * @return The identification number of the record.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Gets the md5sum of the identifier of the taxi - vehicle bound.
	 * @return The md5sum of the identifier of the taxi - vehicle bound.
	 */
	public String getMedallion() {
		return medallion;
	}

	/**
	 * Gets the md5sum of the identifier for the taxi license.
	 * @return The md5sum of the identifier for the taxi license.
	 */
	public String getHackLicense() {
		return hackLicense;
	}

	/**
	 * Gets the time when the passenger(s) were picked up.
	 * @return Time when the passenger(s) were picked up.
	 */
	public Date getPickupDatetime() {
		return pickupDatetime;
	}

	/**
	 * Gets the time when the passenger(s) were dropped off.
	 * @return Time when the passenger(s) were dropped off.
	 */
	public Date getDropoffDatetime() {
		return dropoffDatetime;
	}

	/**
	 * Gets the duration of the trip.
	 * @return Duration of the trip.
	 */
	public double getTripTimeInSecs() {
		return tripTimeInSecs;
	}

	/**
	 * Gets the trip distance in miles.
	 * @return Trip distance in miles.
	 */
	public double getTripDistance() {
		return tripDistance;
	}

	/**
	 * Gets the longitude coordinate of the pickup location.
	 * @return Longitude coordinate of the pickup location.
	 */
	public double getPickupLongitude() {
		return pickupLongitude;
	}

	/**
	 * Gets the latitude coordinate of the pickup location.
	 * @return Latitude coordinate of the pickup location.
	 */
	public double getPickupLatitude() {
		return pickupLatitude;
	}

	/**
	 * Gets the longitude coordinate of the drop-off location.
	 * @return Longitude coordinate of the drop-off location.
	 */
	public double getDropoffLongitude() {
		return dropoffLongitude;
	}

	/**
	 * Gets the latitude coordinate of the drop-off location.
	 * @return Latitude coordinate of the drop-off location.
	 */
	public double getDropoffLatitude() {
		return dropoffLatitude;
	}

	/**
	 * Gets the payment method.
	 * @return The payment method.
	 */
	public PaymentType getPaymentType() {
		return paymentType;
	}

	/**
	 * Gets the fare amount in dollars.
	 * @return Fare amount in dollars.
	 */
	public double getFareAmount() {
		return fareAmount;
	}

	/**
	 * Gets the surcharge in dollars.
	 * @return Surcharge in dollars.
	 */
	public double getSurcharge() {
		return surcharge;
	}

	/**
	 * Gets the tax in dollars.
	 * @return Tax in dollars.
	 */
	public double getMtaTax() {
		return mtaTax;
	}

	/**
	 * Gets the tip in dollars.
	 * @return Tip in dollars.
	 */
	public double getTipAmount() {
		return tipAmount;
	}

	/**
	 * Gets the bridge and tunnel tolls in dollars.
	 * @return Bridge and tunnel tolls in dollars.
	 */
	public double getTollsAmount() {
		return tollsAmount;
	}

	/**
	 * Gets the total paid amount in dollars.
	 * @return Total paid amount in dollars.
	 */
	public double getTotalAmount() {
		return totalAmount;
	}
	
}

