package hr.fer.oop.lab5.task1;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SpringLayout;

import hr.fer.oop.spring.SpringUtilities;

/**
 * Filter dialog class.
 * @author Anthony
 */
public class FilterDialog extends JDialog {
	
	private static final long serialVersionUID = 5860925157766929719L;

	private JCheckBox cbSkipRecords;
	private JLabel lbSkipRecords;
	private JFormattedTextField ftSkipRecords;
	private JCheckBox cbLeaveRecords;
	private JLabel lbLeaveRecords;
	private JFormattedTextField ftLeaveRecords;
	private JCheckBox cbPaymentType;
	private JPanel pnPaymentType;
	private JRadioButton rbCash;
	private JRadioButton rbCard;
	private JRadioButton rbUnknown;
	private JCheckBox cbDistance;
	private JComboBox<String> cmDistance;
	private JFormattedTextField ftDistance;
	
	/**
	 * Creates a new filter dialog.
	 * @param owner Owner of the dialog which will be blocked.
	 */
	public FilterDialog(Frame owner) {
		this(owner, true);
	}
	
	/**
	 * Creates a new filter dialog.
	 * @param owner Owner of the dialog which will be blocked.
	 * @param centerScreen Determines whether the window will start in a center position or not.
	 */
	public FilterDialog(Frame owner, boolean centerScreen) {
		super(owner, false);
		
		cbSkipRecords = new JCheckBox("Skip records?");
		cbSkipRecords.addActionListener(this::cbSkipRecords_Action);
		this.add(cbSkipRecords);
		
		lbSkipRecords = new JLabel("Number of records to skip:");
		this.add(lbSkipRecords);
		
		ftSkipRecords = new JFormattedTextField(NumberFormat.getIntegerInstance());
		ftSkipRecords.setEnabled(false);
		this.add(ftSkipRecords);
		
		cbLeaveRecords = new JCheckBox("Leave records?");
		cbLeaveRecords.addActionListener(this::cbLeaveRecords_Action);
		this.add(cbLeaveRecords);
		
		lbLeaveRecords = new JLabel("Number of records to leave:");
		this.add(lbLeaveRecords);
		
		ftLeaveRecords = new JFormattedTextField(NumberFormat.getIntegerInstance());
		ftLeaveRecords.setEnabled(false);
		this.add(ftLeaveRecords);
		
		cbPaymentType = new JCheckBox("Filter by payment type?");
		cbPaymentType.addActionListener(this::cbPaymentType_Action);
		this.add(cbPaymentType);
		
		this.add(new JPanel());
		
		pnPaymentType = new JPanel();
		pnPaymentType.setBorder(BorderFactory.createTitledBorder("Payment Type?"));
		pnPaymentType.setLayout(new GridLayout(0, 1));
		this.add(pnPaymentType);
		
		rbCash = new JRadioButton("Cash (CSH)");
		rbCash.setEnabled(false);
		pnPaymentType.add(rbCash);
		
		rbCard = new JRadioButton("Card (CRD)");
		rbCard.setEnabled(false);
		pnPaymentType.add(rbCard);
		
		rbUnknown = new JRadioButton("Unknown (UNK)");
		rbUnknown.setEnabled(false);
		pnPaymentType.add(rbUnknown);
		
		ButtonGroup group = new ButtonGroup();
		group.add(rbCash);
		group.add(rbCard);
		group.add(rbUnknown);
		
		cbDistance = new JCheckBox("Limit distance?");
		cbDistance.addActionListener(this::cbDistance_Action);
		this.add(cbDistance);
		
		cmDistance = new JComboBox<>(new String[] { "<", "≥" });
		cmDistance.setEnabled(false);
		this.add(cmDistance);
		
		ftDistance = new JFormattedTextField(NumberFormat.getNumberInstance());
		ftDistance.setEnabled(false);
		this.add(ftDistance);
		
		this.getContentPane().setLayout(new SpringLayout());
		SpringUtilities.makeCompactGrid(this.getContentPane(), 4, 3, 4, 4, 4, 4);
		
		this.setTitle("Taxi Record Filter");
		this.setMinimumSize(new Dimension((int)this.getPreferredSize().getWidth() + 32, (int)this.getPreferredSize().getHeight() + 32));
		this.setMaximumSize(this.getMinimumSize());
		this.setResizable(false);
		if (centerScreen) {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Dimension windowSize = this.getSize();
			this.setLocation((int)(screenSize.getWidth() - windowSize.getWidth()) / 2, (int)(screenSize.getHeight() - windowSize.getHeight()) / 2);
		}
	}
	
	/**
	 * Gets the current filter data.
	 * @return Current filter data.
	 */
	public FilterData getFilterData() {
		FilterData filterData = new FilterData(cbSkipRecords.isSelected(), cbLeaveRecords.isSelected(), cbPaymentType.isSelected(), cbDistance.isSelected());
		if (filterData.useSkipRecords) {
			if (ftSkipRecords.getValue() == null) {
				filterData.useSkipRecords = false;
			} else {
				int value = (int)((Number)ftSkipRecords.getValue()).longValue();
				if (value > 0) {
					filterData.skipRecordsCount = value;
				} else {
					filterData.useSkipRecords = false;
				}
			}
		}
		if (filterData.useLeaveRecords) {
			if (ftLeaveRecords.getValue() == null) {
				filterData.useLeaveRecords = false;
			} else {
				int value = (int)((Number)ftLeaveRecords.getValue()).longValue();
				if (value > 0) {
					filterData.leaveRecordsCount = value;
				} else {
					filterData.useLeaveRecords = false;
				}
			}
		}
		if (filterData.usePaymentType) {
			if (rbCash.isSelected()) {
				filterData.paymentType = TaxiRecord.PaymentType.CASH;
			} else if (rbCard.isSelected()) {
				filterData.paymentType = TaxiRecord.PaymentType.CARD;
			} else if (rbUnknown.isSelected()) {
				filterData.paymentType = TaxiRecord.PaymentType.UNKNOWN;
			} else {
				filterData.usePaymentType = false;
			}
		}
		if (filterData.useLimitDistance) {
			filterData.distanceComparator = FilterData.DistanceComparator.parse((String)cmDistance.getSelectedItem());
			if (ftDistance.getValue() == null) {
				filterData.useLimitDistance = false;
			} else {
				double value = ((Number)ftDistance.getValue()).doubleValue();
				if (value > 0) {
					filterData.distance = value;
				} else {
					filterData.useLimitDistance = false;
				}
			}
		}
		setFilterData(filterData);
		return filterData;
	}
	
	/**
	 * Sets the filter data.
	 * @param filterData Filter data.
	 */
	public void setFilterData(FilterData filterData) {
		if (filterData != null) {
			cbSkipRecords.setSelected(filterData.useSkipRecords);
			ftSkipRecords.setValue(filterData.skipRecordsCount);
			cbLeaveRecords.setSelected(filterData.useLeaveRecords);
			ftLeaveRecords.setValue(filterData.leaveRecordsCount);
			cbPaymentType.setSelected(filterData.usePaymentType);
			switch (filterData.paymentType) {
			case CASH:
				rbCash.setSelected(true);
				break;
			case CARD:
				rbCard.setSelected(true);
				break;
			case UNKNOWN:
				rbUnknown.setSelected(true);
				break;
			}
			cbDistance.setSelected(filterData.useLimitDistance);
			switch (filterData.distanceComparator) {
			case LESSER:
				cmDistance.setSelectedIndex(0);
				break;
			case GREATER_OR_EQUAL:
				cmDistance.setSelectedIndex(1);
				break;
			}
			ftDistance.setValue(filterData.distance);
			cbSkipRecords_Action(null);
			cbLeaveRecords_Action(null);
			cbPaymentType_Action(null);
			cbDistance_Action(null);
		}
	}
	
	private void cbSkipRecords_Action(ActionEvent e) {
		ftSkipRecords.setEnabled(cbSkipRecords.isSelected());
	}
	
	private void cbLeaveRecords_Action(ActionEvent e) {
		ftLeaveRecords.setEnabled(cbLeaveRecords.isSelected());
	}
	
	private void cbPaymentType_Action(ActionEvent e) {
		rbCash.setEnabled(cbPaymentType.isSelected());
		rbCard.setEnabled(cbPaymentType.isSelected());
		rbUnknown.setEnabled(cbPaymentType.isSelected());
	}
	
	private void cbDistance_Action(ActionEvent e) {
		cmDistance.setEnabled(cbDistance.isSelected());
		ftDistance.setEnabled(cbDistance.isSelected());
	}
	
}
