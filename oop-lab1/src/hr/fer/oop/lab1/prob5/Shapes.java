package hr.fer.oop.lab1.prob5;

/**
 * Main class for the Task 5.
 * @author Anthony
 */
public class Shapes {

	/**
	 * Writes  +--------+  to standard output.
	 */
	private static void printPart1() {
		System.out.println("+--------+");
	}
	
	/**
	 * Writes  \        /  to standard output.
	 */
	private static void printPart2() {
		System.out.println("\\        /");
	}
	
	/**
	 * Writes   \______/   to standard output.
	 */
	private static void printPart3() {
		System.out.println(" \\______/ ");
	}
	
	/**
	 * Writes    ______    to standard output.
	 */
	private static void printPart4() {
		System.out.println("  ______  ");
	}
	
	/**
	 * Writes   /      \   to standard output.
	 */
	private static void printPart5() {
		System.out.println(" /      \\ ");
	}
	
	/**
	 * Writes  /        \  to standard output.
	 */
	private static void printPart6() {
		System.out.println("/        \\");
	}
	
	/**
	 * The main entry function of the program.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args) {
		
		/* Shape 1 */
		printPart1();
		printPart2();
		printPart3();
		
		/* Shape 2 */
		printPart4();
		printPart5();
		printPart6();
		printPart1();
		
		/* Shape 3 */
		printPart4();
		printPart5();
		printPart6();
		printPart2();
		printPart3();
		System.out.println();
		
		/* Shape 4 */
		printPart2();
		printPart3();
		printPart1();
		
		/* Shape 5 */
		printPart4();
		printPart5();
		printPart6();
		printPart1();
		
	}
	
}
