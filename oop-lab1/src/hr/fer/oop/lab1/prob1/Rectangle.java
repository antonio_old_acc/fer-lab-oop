package hr.fer.oop.lab1.prob1;

import java.util.Scanner;

/**
 * Main class for the Task 1.
 * @author Anthony
 */
public class Rectangle {
	
	/**
	 * The main entry function of the program.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args) {
		
		double width = -1.0, height = -1.0;
		
		if (args.length == 0) {
			
			Scanner scanner = new Scanner(System.in);
			
			while (width < 0.0) {
				System.out.print("Please provide width: ");
				String input = scanner.nextLine().trim();
				if (input.isEmpty()) {
					System.out.println("The input must not be blank.");
				} else {
					try {
						width = Double.parseDouble(input);
					} catch (Exception ex) {
						/* Invalid width input */
						System.exit(-1);
					}
					if (width < 0.0) {
						System.out.println("The width must not be negative.");
					}
				}
			}
			
			while (height < 0.0) {
				System.out.print("Please provide height: ");
				String input = scanner.nextLine().trim();
				if (input.isEmpty()) {
					System.out.println("The input must not be blank.");
				} else {
					try {
						height = Double.parseDouble(input);
					} catch (Exception ex) {
						/* Invalid height input */
						System.exit(-1);
					}
					if (height < 0.0) {
						System.out.println("The height must not be negative.");
					}
				}
			}
			
			scanner.close();
			
		} else if (args.length == 2) {
			
			try {
				width = Double.parseDouble(args[0]);
				height = Double.parseDouble(args[1]);
				if ((width < 0.0) || (height < 0.0)) {
					/* Negative command-line arguments */
					System.exit(-1);
				}
			} catch (Exception ex) {
				/* Invalid command-line arguments */
				System.exit(-1);
			}
			
		} else {
			
			System.err.println("Invalid number of arguments was provided.");
			System.exit(-1);
			
		}
		
		double area = width * height;
		double perimeter = 2.0 * width + 2.0 * height;
		
		System.out.println("You have specified a rectangle of width " + Double.toString(width) + " and height " + Double.toString(height) + ". Its area is " + Double.toString(area) + " and its perimeter is " + Double.toString(perimeter) + ".");
	}
	
}
