package hr.fer.oop.lab1.prob3;

/**
 * Main class for the Task 3.
 * @author Anthony
 */
public class PrimeNumbers {

	/**
	 * Checks whether a number is prime or not.
	 * @param number A number to check whether it is a prime or not.
	 * @return True if number is prime, false otherwise.
	 */
	private static boolean isPrime(int number) {
		int upperLimit = (int)Math.sqrt((double)number);
		for (int i = 2; i <= upperLimit; i++) {
			if ((number % i) == 0) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * The main entry function of the program.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args) {
		
		int count = 0;
		
		if (args.length == 1) {
			
			try {
				count = Integer.parseInt(args[0]);
				if (count <= 0) {
					System.err.println("Prime number count must be 1 or greater.");
					System.exit(-1);
				}
			} catch (Exception ex) {
				System.err.println("Invalid command-line argument.");
				System.exit(-1);
			}
			
		} else {
			
			System.err.println("You must specify exactly 1 command-line argument.");
			System.exit(-1);
			
		}
		
		System.out.println("You requested calculation of first " + Integer.toString(count) + " prime numbers. Here they are:");
		
		for (int i = 0, x = 2; i < count; x++) {
			if (isPrime(x)) {
				System.out.println(Integer.toString(++i) + ". " + Integer.toString(x));
			}
		}
		
	}
	
}
