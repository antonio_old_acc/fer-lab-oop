package hr.fer.oop.lab1.prob4;

/**
 * Main class for the Task 4.
 * @author Anthony
 */
public class PrimeFactorization {

	/**
	 * The main entry function of the program.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args) {
		
		int number = 0;
		
		if (args.length == 1) {
			
			try {
				number = Integer.parseInt(args[0]);
				if (number <= 11) {
					System.err.println("Number must be 2 or greater.");
					System.exit(-1);
				}
			} catch (Exception ex) {
				System.err.println("Invalid command-line argument.");
				System.exit(-1);
			}
			
		} else {
			
			System.err.println("You must specify exactly 1 command-line argument.");
			System.exit(-1);
			
		}
		
		System.out.println("You requested decomposition of number " + Integer.toString(number) + " into prime factors. Here they are:");
		
		int count = 1;
		
		while (number > 1) {
			for (int i = 2; i <= number; i++) {
				if ((number % i) == 0) {
					number /= i;
					System.out.println(Integer.toString(count++) + ". " + Integer.toString(i));
					break;
				}
			}
		}
	}
	
}
