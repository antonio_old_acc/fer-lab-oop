package hr.fer.oop.lab1.prob6;

/**
 * Element of the binary tree.
 * @author Anthony
 */
public class TreeNode {
	
	TreeNode left;
	TreeNode right;
	String data;
	
	/**
	 * Constructs a new tree node element with string data.
	 * @param data Value of the tree node.
	 */
	public TreeNode(String data) {
		left = null;
		right = null;
		this.data = data;
	}
	
}
