package hr.fer.oop.lab1.prob6;

/**
 * Binary tree implementation.
 * @author Anthony
 */
public class BinaryTree {
	
	TreeNode root;
	
	/**
	 * Constructs an empty binary tree.
	 */
	public BinaryTree() {
		root = null;
	}
	
	/**
	 * Inserts an element into the binary tree. Lesser strings are on the left while greater or equal ones are on the right.
	 * @param data Value that should be inserted.
	 */
	public void insert(String data) {
		TreeNode node = new TreeNode(data);
		if (root == null) {
			root = node;
		} else {
			TreeNode current = root;
			while (true) {
				if (data.compareTo(current.data) < 0) {
					if (current.left == null) {
						current.left = node;
						break;
					} else {
						current = current.left;
					}
				} else {
					if (current.right == null) {
						current.right = node;
						break;
					} else {
						current = current.right;
					}
				}
			}
		}
	}
	
	/**
	 * Searches whether the binary tree contains a value in the node.
	 * @param node Tree node to search.
	 * @param data Value to find.
	 * @return True when value is contained within node, false otherwise.
	 */
	private boolean subTreeContainsData(TreeNode node, String data) {
		if (node == null) {
			return false;
		}
		if (node.data.equals(data)) {
			return true;
		}
		if (data.compareTo(node.data) < 0) {
			return subTreeContainsData(node.left, data);
		} else {
			return subTreeContainsData(node.right, data);
		}
	}
	
	/**
	 * Searches whether the binary tree contains a value in the root node.
	 * @param data Value to find.
	 * @return True when value is contained within the root node, false otherwise.
	 */
	public boolean containsData(String data) {
		return subTreeContainsData(root, data);
	}
	
	/**
	 * Searches whether the binary tree contains a value in the node (regardless of how the items are sorted).
	 * @param node Tree node to search.
	 * @param data Value to find.
	 * @return True when value is contained within node, false otherwise.
	 */
	private boolean subTreeContainsData2(TreeNode node, String data) {
		if (node == null) {
			return false;
		}
		if (node.data.equals(data)) {
			return true;
		}
		return (subTreeContainsData2(node.left, data) || subTreeContainsData2(node.right, data));
	}
	
	/**
	 * Searches whether the binary tree contains a value in the root node (regardless of how the items are sorted).
	 * @param data Value to find.
	 * @return True when value is contained within the root node, false otherwise.
	 */
	public boolean containsData2(String data) {
		return (subTreeContainsData2(root.left, data) || subTreeContainsData2(root.right, data));
	}
	
	/**
	 * Counts the size of a specific node.
	 * @param node Node which size is to be counted.
	 * @return Node size.
	 */
	private int sizeOfSubTree(TreeNode node) {
		if (node == null) {
			return 0;
		}
		return 1 + sizeOfSubTree(node.right) + sizeOfSubTree(node.left);
	}
	
	/**
	 * Counts the size of the tree node.
	 * @return Root node size.
	 */
	public int sizeOfTree() {
		return sizeOfSubTree(root);
	}
	
	/**
	 * Writes tree node and its children in-order.
	 * @param node Node which will be written.
	 */
	private void writeSubTree(TreeNode node) {
		if (node == null) {
			return;
		}
		writeSubTree(node.left);
		System.out.print(node.data + " ");
		writeSubTree(node.right);
	}
	
	/**
	 * Writes the root node and its children in-order.
	 */
	public void writeTree() {
		writeSubTree(root);
		System.out.println();
	}
	
	/**
	 * Reverses the order of a specific tree node.
	 * @param node Node whose order should be reversed.
	 */
	private void reverseSubTreeOrder(TreeNode node) {
		if (node == null) {
			return;
		}
		TreeNode leftTemp = node.left;
		node.left = node.right;
		node.right = leftTemp;
		reverseSubTreeOrder(node.left);
		reverseSubTreeOrder(node.right);
	}
	
	/**
	 * Reverses the entire tree order.
	 */
	public void reverseTreeOrder() {
		reverseSubTreeOrder(root);
	}
	
	/**
	 * The main entry function of the program.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args) {
		BinaryTree tree = new BinaryTree();
		tree.insert("Jasna");
		tree.insert("Ana");
		tree.insert("Ivana");
		tree.insert("Anamarija");
		tree.insert("Vesna");
		tree.insert("Kristina");
		System.out.println("Writing tree inorder:");
		tree.writeTree();
		tree.reverseTreeOrder();
		System.out.println("Writing reversed tree inorder:");
		tree.writeTree();
		int size = tree.sizeOfTree();
		System.out.println("Number of nodes in tree is " + size + ".");
		/* Should use containsData2 */
		boolean found = tree.containsData("Ivana");
		System.out.println("Searched element is found: " + found);
	}

}

