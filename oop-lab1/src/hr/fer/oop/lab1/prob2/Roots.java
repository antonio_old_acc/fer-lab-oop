package hr.fer.oop.lab1.prob2;

import java.text.DecimalFormat;

/**
 * Main class for the Task 2.
 * @author Anthony
 */
public class Roots {

	/**
	 * The main entry function of the program.
	 * @param args Command-line arguments array.
	 */
	public static void main(String[] args) {
		
		double x = 0.0, y = 0.0;
		int numRoots = 0;
		
		if (args.length == 3) {
			try {
				x = Double.parseDouble(args[0]);
				y = Double.parseDouble(args[1]);
				numRoots = Integer.parseInt(args[2]);
				if (numRoots <= 1) {
					System.err.println("Power of root must be 2 or greater.");
					System.exit(-1);
				}
			} catch (Exception ex) {
				System.err.println("Invalid command-line argument(s).");
				System.exit(-1);
			}
		} else {
			System.err.println("You must specify exactly 3 command-line arguments.");
			System.exit(-1);
		}
		
		System.out.println("You requested calculation of " + Integer.toString(numRoots) + ". roots. Solutions are: ");
		
		double r = Math.sqrt(Math.pow(x, 2.0) + Math.pow(y, 2.0));
		double phi = Math.atan2(y, x);
		
		double rootR = Math.pow(r, 1.0 / (double)numRoots);
		
		for (int k = 0; k < numRoots; k++) {
			
			DecimalFormat df = new DecimalFormat("0.##");
			
			double rootX = rootR * Math.cos((phi + 2.0 * (double)k * Math.PI) / (double)numRoots);
			double rootY = rootR * Math.sin((phi + 2.0 * (double)k * Math.PI) / (double)numRoots);
			
			System.out.println(Integer.toString(k + 1) + ") " + df.format(rootX) + ((rootY >= 0.0) ? " + " : " - ") + df.format(Math.abs(rootY)) + "i");
			
		}
		
	}
	
}
