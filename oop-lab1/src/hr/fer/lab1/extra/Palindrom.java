package hr.fer.lab1.extra;

/**
 * Task 1 class
 * @author Anthony
 */
public class Palindrom {

	/**
	 * Function checks whether a string is valid as a palindrome(?)
	 * @param str
	 * @return
	 */
	public static boolean check(String str) {
		int length = str.length();
		int halfLength = length / 2;
		for (int i = 0; i < halfLength; i++) {
			if (str.charAt(i) != str.charAt(length - i - 1)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Entry function/method
	 * @param args Command-line arguments
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("There must be exactly 1 argument.");
			System.exit(-1);
		}
		System.out.println("Input: " + args[0]);
		System.out.println("Output: " + check(args[0]));
	}
	
}
