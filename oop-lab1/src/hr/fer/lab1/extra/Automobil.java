package hr.fer.lab1.extra;

/**
 * Task 2 class
 * @author Anthony
 */
public class Automobil {

	private double totalKilometers;
	private double totalGasCapacity;
	private double currentGasQuantity;
	private double gasUsage;
	
	/**
	 * Constructor for 'Automobil' class
	 * @param totalGasCapacity Max amount (in litres) of gas
	 * @param gasUsage Amount (in litres/100km) of gas usage
	 */
	public Automobil(double initialKm, double currentFuel, double totalGasCapacity, double gasUsage) {
		totalKilometers = initialKm;
		this.totalGasCapacity = totalGasCapacity;
		currentGasQuantity = currentFuel;
		this.gasUsage = gasUsage;
	}
	
	/**
	 * Commands the vehicle to drive an amount of kilometers (if not enough fuel is available, it will drive the max distance)
	 * @param km Amount of kilometers to drive
	 */
	public void vozi(double km) {
		double gasNeeded = gasUsage * (km / 100.0);
		if (gasNeeded > currentGasQuantity) {
			gasNeeded = currentGasQuantity;
			km = gasNeeded / (gasUsage / 100.0);
		}
		currentGasQuantity -= gasNeeded;
		totalKilometers += km;
		System.out.println("Distance driven: " + Double.toString(km) + " km");
		System.out.println();
	}
	
	/**
	 * Refills the fuel (if fuel cannot fit in the tank, it will be filled to 100%)
	 * @param gorivo Amount of fuel in litres to fill
	 */
	public void natoci(double gorivo) {
		if (currentGasQuantity + gorivo > totalGasCapacity) {
			gorivo = totalGasCapacity - currentGasQuantity;
		}
		currentGasQuantity += gorivo;
		System.out.println("Refilled " + Double.toString(gorivo) + " litre(s)");
		System.out.println();
	}
	
	/**
	 * Prints some vehicle statistics:
	 *  - total kilometers driven
	 *  - fuel (percentage)
	 *  - kilometers remaining
	 */
	public void ispisPutnoRacunalo() {
		System.out.println("Total kilometers driven: " + Double.toString(totalKilometers) + " km");
		System.out.println("Fuel: " + Double.toString(currentGasQuantity * 100.0 / totalGasCapacity) + "%");
		System.out.println("Kilometers remaining: " + Double.toString(currentGasQuantity / (gasUsage / 100.0)));
		System.out.println();
	}
	
	/**
	 * Entry function/method for Task 2
	 * @param args Command-line arguments
	 */
	public static void main(String[] args) {
		Automobil vehicle = new Automobil(100000.0, 5.0, 60.0, 5.5);
		vehicle.ispisPutnoRacunalo();
		vehicle.vozi(200.0); // potrosi 11 litara
		vehicle.ispisPutnoRacunalo();
		vehicle.natoci(15.0); // moze samo 11 do kapaciteta
		vehicle.ispisPutnoRacunalo();
		vehicle.vozi(1500.0); // moze samo 1090.90909... km
		vehicle.ispisPutnoRacunalo();
		vehicle.natoci(100.0); // moze voziti jos 400 km
		vehicle.ispisPutnoRacunalo();
	}
	
}
