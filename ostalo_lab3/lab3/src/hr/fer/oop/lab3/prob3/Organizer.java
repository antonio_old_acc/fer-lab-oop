/**
 * 
 */
package hr.fer.oop.lab3.prob3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.List;

/**
 * Simple class implementing the functionality requested by the 3rd assignment
 * in preparation for the laboratory work.
 * <p>Instances of this class encapsulate the set of data read from the input 
 * and consisting of the students' names and grades. Internally a LinkedHashMap
 * is used to preserve the student's input order. 
 * <p>An inner class is used to store the grades of each student. The class is 
 * open for overrides if proven necessary to handle expanded input data.
 * <p>The statistics counting and outputs are handled in static methods of the
 * class.
 * 
 * @author OOPJ
 *
 */
public class Organizer {

	public static final String VERSION = "v42";

	private LinkedHashMap<String, GradeList> studentGrades = new LinkedHashMap<>();

	public Organizer() {}

	public Organizer(InputStream is) { 
		load(this, is);
	}

	/**
	 * Add a single grade for a student. If the Student instance isn't found 
	 * in the table, a new key-value pair will be added, otherwise the grade 
	 * will be added to the existing list of grades.
	 * 
	 * 
	 * @param student
	 * @param grade
	 */
	public void addGrade(String student, int grade) {
		if(student == null) { 
			throw new IllegalArgumentException();
		}
		if(studentGrades.containsKey(student)) { 
			studentGrades.get(student).grades.add(grade);
		}
		else {
			studentGrades.put(student, new GradeList(student, grade));
		}
	}

	/**
	 * Get the grades for a certain student. Returns null for null-arguments
	 * or if the requested Student instance wasn't found.
	 * 
	 * @param student
	 * @return
	 */
	public List<Integer> getGrades(String student) {
		if(student == null || !studentGrades.containsKey(student)) { return null; }
		GradeList list = studentGrades.get(student);
		if(list == null) { return null; }
		return new Vector<Integer>(list.grades);
	}

	/*
	 * The inner class allows for possible overrides through classes keeping 
	 * an expanded list of attributes. In such cases, additional overrides in
	 * the outer class will also need to be implemented.
	 */
	protected static class GradeList {
		
		protected final String student;
		protected Vector<Integer> grades = new Vector<>();

		protected GradeList(String student) { 
			this.student = student;			
		}
		protected GradeList(String student, Integer firstGrade) { 
			this(student);
			grades.add(firstGrade);
		}
	}

	public LinkedHashMap<String, GradeList> getAllGrades() { 
		return new LinkedHashMap<String, GradeList>(studentGrades);
	}


	public static void main(String[] args) { 
		Organizer myOrganizer = new Organizer();
		InputStream in = System.in;
		PrintStream out = System.out;
		/* 
		 * parsing options
		 * 
		 * for a more realistic, complete and less cumbersome get-opts   
		 * implementation, see args4j (http://args4j.kohsuke.org/) or 
		 * commons-cli (https://commons.apache.org/proper/commons-cli/)
		 *  
		 * */
		List <String> argsList = Arrays.asList(args);		
		if(argsList.contains("-v") || argsList.contains("--version")) {
			if(args.length == 1) {
				printVersion();
				System.exit(0);
			}
			else {
				System.err.println("No other arguments allowed with -v, -h, --version, --help");
				System.exit(0);
			}
		}
		if(argsList.contains("-h") || argsList.contains("--help")) { 
			if(args.length == 1) {
				printHelpAndExit();
			}
			else {
				System.err.println("No other arguments allowed with -v, -h, --version, --help");
				System.exit(0);
			}
		}

		String inName = null, outName = null;
		try {
			if(argsList.contains("-i")) {
				inName =argsList.get(argsList.indexOf("-i") + 1);
			}
			if(argsList.contains("-o")) { 
				outName = argsList.get(argsList.indexOf("-o") + 1);
			}
			for(String arg : argsList) { 
				if(arg.startsWith("--input=")) { 
					if(inName != null) { 
						printHelpAndExit("Only one of -i or --input=inputfile allowed"); 
					}					
					inName = arg.substring("--input=".length());
				}
				if(arg.startsWith("--output=")) { 
					if(outName != null) { 
						printHelpAndExit("Only one of -o or --output=outpufile allowed"); 
					}					
					outName = arg.substring("--output=".length());
				}
			}
		}
		catch(IndexOutOfBoundsException e) { 
			printHelpAndExit("Valid filename required after -i or -o");
		}

		try { 
			if(inName != null) { in = new FileInputStream(inName); }
			if(outName != null) { out = new PrintStream(outName); }
		}
		catch(FileNotFoundException e) { 
			System.err.println("Can't find requested file, caught exception: " + e.getMessage());
			System.exit(1);
		}
		catch(SecurityException e) { 
			System.err.println("Can't access requested file, caught exception: " + e.getMessage());
			System.exit(2);

		}
		load(myOrganizer, in);
		printStats(myOrganizer, out);
	}

	public static void printHelpAndExit(String prefixText) { 
		System.err.println(prefixText);
		System.err.println();
		printHelpAndExit();
	}
	
	/*
	 * The switches are implemented (incompletely - e.g. unrecognized switches 
	 * are tolerated if not invoked in one of the single-switch modes i.e. 
	 * help or version mode) to allow for a more civilized program behavior as
	 * well as to introduce the concept to students new to the form. 
	 */
	public static void printHelpAndExit() {
		System.err.println("Organizer");
		System.err.println("  A small program to demonstrate the basic knowledge of");
		System.err.println("  working with the Java Collections Framework");
		System.err.println("  For fun, the program supports basic gnu-like and POSIX-like options");
		System.err.println("  Students are encouraged to pipe the program input from a file not by");
		System.err.println("  using the -i option but a Unix-style pipe and similarly to pipe the output");
		System.err.println("Usage:");
		System.err.println("    Organizer [-i inputfile|--input=inputfile] [-o outputfile|--output=outputfile");		
		System.err.println("    Organizer [-h|--help]");
		System.err.println("    Organizer [-v|--version]");
		System.err.println("Options:");
		System.err.println("    -i|--input=filename");		
		System.err.println("           if used with -i or it's double-dash counterpart, the program will");
		System.err.println("           read input from a file");
		System.err.println("    -o|--output=filename");
		System.err.println("           if used with -o, output will be printed to a file");
		System.err.println("    -h|--help");
		System.err.println("           Prints out this help message");
		System.err.println("    -v|--version");
		System.err.println("           Prints out the version of the program");		
		System.exit(0);
	}

	public static void printVersion() { 
		System.out.println("Organizer version \"" + VERSION + "\"");
	}

	public static void load(Organizer o, InputStream is) { 
		try (Scanner s = new Scanner(is)) {
			do {
				try {
					String nl= s.nextLine();
					if("KRAJ".equals(nl)) { 
						break;
					}
					StringTokenizer st = new StringTokenizer(nl);
					String name = st.nextToken();
					int grade = Integer.valueOf(st.nextToken());
					o.addGrade(name, grade);					
				}
				catch(NumberFormatException|NoSuchElementException e) {
					System.err.println("Exception reading line, will continue...");
					e.printStackTrace();
				}

			} while(s.hasNextLine());			
		}
	}

	public static void printStats(Organizer o, PrintStream out) { 
		LinkedHashMap<String,GradeList> allGrades = o.getAllGrades();
		for(String student : allGrades.keySet()) {
			Vector<Integer> studentGrades = allGrades.get(student).grades;
			double average = 0, deviation = 0;
			StringBuilder sb = new StringBuilder("U�enik ").append(student)
					.append("\n  Broj ocjena:").append(studentGrades.size())
					.append("\n  Ocjene: ");
			HashSet<Integer> distinct = new HashSet<Integer>();
			for(Integer grade : studentGrades) {
				average += grade;
				distinct.add(grade);
				sb.append(grade).append(" ");
			}
			sb.deleteCharAt(sb.length()-1);
			average /= studentGrades.size();

			for(Integer grade : studentGrades) { 
				deviation += (average - grade) * (average - grade); 
			}
			deviation = Math.sqrt(deviation / studentGrades.size());

			sb.append("\n  Razli�ite ocjene: ");
			for(Integer ocjena : distinct) { 
				sb.append(ocjena).append(" ");
			}
			sb.deleteCharAt(sb.length()-1);
			sb.append("\n  Prosje�na ocjena: %5.2f\n  Standardno odstupanje: %5.2f\n\n");
			out.format(sb.toString(), average, deviation);



		}	
	}

}
