package hr.fer.oop.lab4.third;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * @author OOP
 */
public class Program {

	public static void main(String[] args) {

		Path fileToRewrite = Paths.get("racuni/2003/1/Racun_0.txt");
		Path crypted = Paths.get("cripted.txt");
		Path uncrypted = Paths.get("uncripted.txt");
				
		try{
			InputStream is= Files.newInputStream(fileToRewrite, StandardOpenOption.READ);
			MyCriptByteWriter rewriter = new MyCriptByteWriter(is,crypted);
			rewriter.run();
			is.close();
			
			InputStream is1= Files.newInputStream(crypted, StandardOpenOption.READ);
			MyCriptByteWriter rewriter1 = new MyCriptByteWriter(is1,uncrypted);
			rewriter1.run();
			is1.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
