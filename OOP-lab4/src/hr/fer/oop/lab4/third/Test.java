package hr.fer.oop.lab4.third;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author OOP
 */
public class Test {
	
	public static void main(String[] args) {
		final Path pocetna = Paths.get("racuni/2003/1/Racun_0.txt");
		final Path konacna = Paths.get("uncripted.txt");

		try {			
			List<String> s1 = Files.readAllLines(pocetna);
			List<String> s2 = Files.readAllLines(konacna);
			long sz1 = Files.size(pocetna);
			long sz2 = Files.size(konacna);
			
			if(s1.equals(s2)){
				System.out.println("Racuni su jednaki");
			} else {
				System.out.println("Racuni nisu jednaki");
			}
			
			if(sz1 == sz2){
				System.out.println("Veličine datoteka su jednake");
			} else {
				System.out.println("Veličine datoteka nisu jednake");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
				
	}
	
	

	
	

}
