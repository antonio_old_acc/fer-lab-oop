package hr.fer.oop.lab4.third;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author OOP
 */
public class MaskStream extends OutputStream{
	private OutputStream stream;
	private byte x;
	
	public MaskStream(OutputStream stream, byte x){
		this.stream = stream;
		this.x = x;
	}
	
	@Override
	public void write(int b) throws IOException {
		stream.write(b ^ x);
	}
	
}
