package hr.fer.oop.lab4.third;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import hr.fer.oop.lab4.first.a.MyByteWriter;

/**
 * @author OOP
 */
public class MyCriptByteWriter extends MyByteWriter{

	public MyCriptByteWriter(InputStream inputStream, Path newFile) {
		super(inputStream, newFile);
	}

	@Override
	public void run() {
		if(Files.notExists(this.newFile, LinkOption.NOFOLLOW_LINKS)){
			this.createFile(this.newFile);
		}
		
		try(OutputStream os= new MaskStream(Files.newOutputStream(this.newFile, StandardOpenOption.WRITE),(byte) 0xF0)){
			byte [] buffer = new byte[1];
			BufferedInputStream bis = new BufferedInputStream(this.inputStream);
			while(true){
				int numOfReadBytes = bis.read(buffer);
				if(numOfReadBytes<1) break;
				os.write(buffer, 0, numOfReadBytes);
			}
			bis.close();
			this.inputStream.close();
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
}
