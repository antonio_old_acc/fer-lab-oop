package hr.fer.oop.lab4.first.a;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

/**
 * @author OOP
 */
public class SingleFileProgram {

	public static void main(String[] args) {
		
		Path fileToRewrite = Paths.get("racuni/2003/1/Racun_0.txt"); //original file (source)
		Path result = Paths.get("rewritten-racun0.txt"); // file where original will be duplicated (destination)
		
		try(InputStream is= Files.newInputStream(fileToRewrite, StandardOpenOption.READ)){
			MyByteWriter rewriter = new MyByteWriter(is,result);
			rewriter.run();
			System.out.println(filesEquals(fileToRewrite, result)); //true if files contain the same content
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static boolean filesEquals(Path f1, Path f2) throws IOException {
        return Arrays.equals(Files.readAllBytes(f1), Files.readAllBytes(f2));
    }

}
